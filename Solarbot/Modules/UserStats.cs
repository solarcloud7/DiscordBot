﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Models;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solarbot.Modules
{

    public class Stats : BaseSolarbotModule
    {
        [Command("guildstats1")]
        [Alias("gs1")]
        public async Task InfoAsync(int days = 30)
        {
            try
            {
                var guildId = Context.Guild.Id;
                if (guildId == 0)
                {
                    await ReplyAsync("Use command from inside a Discord Guild");
                    return;
                }

                using (var context = new BotContext())
                {
                    var cUsers = (Context.Channel as SocketTextChannel)?.Users;

                    var members = context.MemberStats.Include(x => x.User).Where(ms =>
                        cUsers.Any(x => x.Id == ms.UserId && x.IsBot == false))
                        .GroupBy(m => m.UserId)
                        .ToList()
                        .Select(m => new MemberStatsModel(m, days))
                        .Order();

                    var g = new Graph();
                    g.SetTitle($"{Context.Guild.Name} Growth " + members.Dates())
                        .SetBottomLabels(new List<string> { "Level Percent Increase", "Influence Percent Increase" })
                        .SetAxisRange(0, 0, 100)
                        .SetData(members.LevelPerc(), members.InfluencePerc())
                        .SetAxisLabels(0, members.Members())
                        .SetChartSize(600, 999)
                        .SetMargin(0, 40);

                    var message = $"```prolog\n Comparative Stats from the past {days}x days.```";
                    await g.SendChartAsync(Context.Channel, g.ToUrl(), message);
                }
                //Debug delete message
                //await Context.Message.DeleteAsync();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("GuildStats2")]
        [Alias("gs2")]
        public async Task GuildStats2Async(int days = 30)
        {
            try
            {
                var guildId = Context.Guild.Id;
                if (guildId == 0)
                {
                    await ReplyAsync("Use command from inside a Discord Guild");
                    return;
                }

                using (var context = new BotContext())
                {
                    var cUsers = (Context.Channel as SocketTextChannel)?.Users;

                    var members = context.MemberStats.Include(x => x.User).Where(ms =>
                        cUsers.Any(x => x.Id == ms.UserId && x.IsBot == false))
                        .GroupBy(m => m.UserId)
                        .ToList()
                        .Select(m => new MemberStatsModel(m, days))
                        .Order();

                    var g = new Graph();
                    g.SetTitle($"{Context.Guild.Name} " + members.Dates())
                        .SetType("bhs")
                        .SetBottomLabels(new List<string> { "Level Percent Increase", "Influence Percent Increase" })
                        .SetAxisRange(0, 0, 100)
                        .SetData(members.LevelPerc(), members.InfluencePerc())
                        .SetAxisLabels(0, members.Members())
                        .SetChartSize(600, 999)
                        .SetMargin(0, 40);

                    var message = $"```prolog\n Comparative Stats from the past {days}x days.```";
                    await g.SendChartAsync(Context.Channel, g.ToUrl(), message);

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("guildstats3")]
        [Alias("gs3")]
        public async Task StatsAsync(int days = 30)
        {
            try
            {
                var guildId = Context.Guild.Id;
                if (guildId == 0)
                {
                    await ReplyAsync("Use command from inside a Discord Guild");
                    return;
                }

                using (var context = new BotContext())
                {
                    var cUsers = (Context.Channel as SocketTextChannel)?.Users;

                    var members = context.MemberStats.Include(x => x.User)
                        .Where(ms => cUsers.Any(x => x.Id == ms.UserId && x.IsBot == false))
                        .GroupBy(m => m.UserId)
                        .ToList()
                        .Select(m => new MemberStatsModel(m, days))
                        .Order();

                    var table = new Table("Name", "Level", "Influence");

                    foreach (var m in members)
                    {
                        table.AddRow(m.Name.FirstLetterToUpperCase(), $"{m.CurrentMemberStat.Level}  +({m.LevelProgress})", $"{ m.CurrentMemberStat.Influence }  +({ m.InfluenceProgress})");
                    }
                    //var text = $"{Context.Guild.Name} {members.Dates()} \n";
                    //await ReplyAsync(text + table.ToString());
                    var message = $"```prolog\n Stats from the past {days}x days.```";
                    foreach (string chunk in table.AsStringChunks(message))
                    {
                        await ReplyAsync(chunk);
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        #region Individual Stat
        [Command("stat")]
        [Alias("s")]
        [Remarks("Individual stat does something, probably")]
        public async Task InfoAsync(string text, int days = 30)
        {
            try
            {
                using (var context = new BotContext())
                {
                    List<MemberStats> list = new List<MemberStats>();
                    if (Context.Message.MentionedUsers.Count > 0)
                    {
                        var id = Context.Message.MentionedUsers.ElementAt(0).Id;
                        list = context.MemberStats
                                .Where(m => m.UserId == id)
                                .ToList();

                        if (list.Count == 0)
                        {
                            await ReplyAsync($"Never seen <@{id}> before.");
                            return;
                        }
                    }
                    else
                    {
                        list = context.MemberStats
                                 .Where(m => string.Equals(m.Nickname, text, StringComparison.OrdinalIgnoreCase) ||
                                             string.Equals(m.UserName, text, StringComparison.OrdinalIgnoreCase))
                                 .ToList();


                        if (list.Count == 0)
                        {
                            await ReplyAsync($"Never seen {text} before.");
                            return;
                        }
                    }

                    var member = new MemberStatsModel(list.ToList(), days);

                    //LEVEL
                    var lg = new Graph();
                    lg.SetTitle($"{member.Name} {member.DatesText}")
                        .SetType(GraphType.Line)
                        .SetBottomLabels(new List<string>() { $"gained+{member.LevelProgress}+levels+to+a+total+of+{member.CurrentMemberStat.Level}" })
                        .SetAxisRange(1, member.Levels.Min() - 1, member.Levels.Max() + 1)
                        .SetData(member.Levels)
                        .SetAxisLabels(0, member.Dates)
                        .SetChartSize(600, 200)
                        .SetMargin(0, 40);

                    var ig = new Graph();
                    ig.SetTitle($"{member.Name} {member.DatesText}")
                        .SetType(GraphType.Line)
                        .SetBottomLabels(new List<string>() { $"gained+{member.InfluenceProgress}+influence+to+a+total+of+{member.CurrentMemberStat.Influence}" })
                        .SetAxisRange(1, member.Influences.Min() - 50, member.Influences.Max() + 50)
                        .SetData(member.Influences)
                        .SetAxisLabels(0, member.Dates)
                        .SetChartSize(600, 200)
                        .SetLineFills("B,3fb560,0,0,0")
                        .SetMargin(0, 40);

                    var message = $"```prolog\n Stats from the past {days}x days.```";
                    await Graph.CombineAsync(new List<string>() { lg.ToUrl(), ig.ToUrl() }, Context.Channel, message);
                    // await g.SendChartAsync(Context.Channel, g.ToUrl());

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region Log Stat
        [Command("logstat")]
        [Alias("ls")]
        public async Task LogStatAsync(int level, int influence)
        {
            //validation
            level = (level < 0) ? 0 : level;
            influence = (influence < 0) ? 0 : influence;

            var guildId = Context.Guild.Id;
            if (guildId == 0)
            {
                await ReplyAsync("Use command from inside a Discord Guild");
                return;
            }
            var user = Context.User as SocketGuildUser;

            if (SaveStat(user, level, influence))
            {
                var name = user.Nickname ?? user.Username;
                var message = $"{name} has updated his stats to Level:{level} and Influence:{influence}!";

                await ReplyAsync(message);
            }
        }

        [Command("logstat")]
        [Alias("ls")]
        public async Task LogStatAsync(SocketGuildUser user, int level, int influence)
        {
            //validation
            level = (level < 0) ? 0 : level;
            influence = (influence < 0) ? 0 : influence;

            var guildId = Context.Guild.Id;
            if (guildId == 0)
            {
                await ReplyAsync("Use command from inside a Discord Guild");
                return;
            }

            if (SaveStat(user, level, influence))
            {
                var name = user.Nickname ?? user.Username;
                var message = $"{name} has updated his stats to Level:{level} and Influence:{influence}!";

                await ReplyAsync(message);
            }
        }

        private bool SaveStat(SocketGuildUser user, int level, int influence)
        {
            try
            {
                using (var context = new BotContext())
                {
                    //check if stat has been created by member today already
                    var todayStat = context.MemberStats
                        .FirstOrDefault(m => m.UserId == user.Id &&
                                    m.CreatedDate.Date == DateTime.Today.Date);

                    //if not,  create one,  else update one.
                    if (todayStat == null)
                    {
                        context.MemberStats.Add(new MemberStats
                        {
                            CreatedDate = DateTime.Now,
                            Influence = influence,
                            Level = level,
                            UserId = user.Id,
                            Nickname = user.Nickname,
                            UserName = user.Username,
                            GuildId = Context.Guild.Id
                        });
                    }
                    else
                    {
                        todayStat.CreatedDate = DateTime.Now;
                        todayStat.Influence = influence;
                        todayStat.Level = level;
                        todayStat.UserId = user.Id;
                        todayStat.Nickname = user.Nickname;
                        todayStat.UserName = user.Username;
                        todayStat.GuildId = Context.Guild.Id;
                    }
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        #endregion


        #region Del Stat
        [Command("delstat")]
        [Alias("ds")]
        public async Task DelLogAsync()
        {
            try
            {
                using (var context = new BotContext())
                {
                    //check if stat has been created by member today already
                    var todayStat = context.MemberStats
                        .FirstOrDefault(m => m.UserId == Context.User.Id &&
                                    m.CreatedDate.Date == DateTime.Today.Date);

                    //delete if has a stat,  else error message
                    if (todayStat == null)
                    {
                        await ReplyAsync($"A statistic does not exist today for {Context.User.Username}.");
                        return;
                    }
                    else
                    {
                        context.MemberStats.Remove(todayStat);
                        await context.SaveChangesAsync();

                        await ReplyAsync($"{Context.User.Username}'s statistic for today has been deleted.");
                    }

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("delstat")]
        [Alias("ds")]
        public async Task DelLogAsync(SocketGuildUser user)
        {
            try
            {
                using (var context = new BotContext())
                {
                    //check if stat has been created by member today already
                    var todayStat = context.MemberStats
                        .FirstOrDefault(m => m.UserId == user.Id &&
                                    m.CreatedDate.Date == DateTime.Today.Date);

                    //delete if has a stat,  else error message
                    if (todayStat == null)
                    {
                        await ReplyAsync($"A statistic does not exist today for {user.Username}.");
                        return;
                    }
                    else
                    {
                        context.MemberStats.Remove(todayStat);
                        await context.SaveChangesAsync();

                        await ReplyAsync($"{user.Username}'s statistic for today has been deleted.");
                    }

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        [Command("seen")]
        public async Task SeenAsync(string text)
        {
            if (Context.Message.MentionedUsers.Count > 0)
            {
                var id = Context.Message.MentionedUsers.ElementAt(0).Id;
                await ReplyAsync(UserManager.Seen(id));
            }
            else
            {
                await ReplyAsync(UserManager.Seen(text));
            }

            await Context.Message.DeleteAsync();
        }

        [Command("cseen")]
        public async Task ChannelSeenAsync()
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var cUsers = (Context.Channel as SocketTextChannel)?.Users;

            var table = new Table("Name", "Last Seen");

            using (var context = new BotContext())
            {
                var now = DateTime.Now;
                Dictionary<string, DateTime> list = new Dictionary<string, DateTime>();

                foreach (var user in cUsers)
                {
                    var dbUser = context.Users.FirstOrDefault(u => u.Id == user.Id);
                    if (dbUser == null)
                        continue;

                    list.Add(
                        dbUser.DisplayName,
                        UserManager.IsOnline(user) ? now : dbUser.LastSeen);
                }

                list.OrderByDescending(x => x.Value)
                    .ForEach(x => table.AddRow(x.Key, x.Value.DisplayTime()));
            }

            foreach (var chunk in table.AsStringChunks())
            {
                await ReplyAsync(chunk);
            }

            await Context.Message.DeleteAsync();
        }
    }
}