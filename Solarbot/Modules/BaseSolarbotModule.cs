﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solarbot.Exceptions;
using Solarbot.Models;

namespace Solarbot.Modules
{
    public class BaseSolarbotModule : ModuleBase<SocketCommandContext>
    {
        /**
         * Retrieve the User object from the Discord Context environment.
         * 
         * This will either contain the Guild (server) of the user OR will throw an Exception
         * informing the user that they need to set their default server.
         */
        protected Users GetUserFromContext()
        {
            ulong guildId = 0;
            Users user;
            UserDefaultGuilds ug;

            using (var context = new BotContext())
            {
                // The UserDefaultGuild will either be the source of the lookup on private messages, or will get updated on regular.
                ug = context.UserDefaultGuilds.SingleOrDefault(m => m.UserId == Context.User.Id);

                if (Context.IsPrivate)
                {
                    // Private messages require using the UserDefaultGuild table first, as the user may have an account on multiple servers.
                    if (ug != null)
                    {
                        guildId = ug.GuildId;
                    }
                }
                else
                {
                    // Easy one, direct messages from a guild server will have its guild available ;)
                    guildId = Context.Guild.Id;

                    // Remember this for the next run too, to make PM's work more smoothly.
                    if (ug == null)
                    {
                        context.UserDefaultGuilds.Add(
                            new UserDefaultGuilds { UserId = Context.User.Id, GuildId = Context.Guild.Id }
                        );
                    }
                    else
                    {
                        ug.GuildId = Context.Guild.Id;
                    }

                    // We'll just stash this default group real quick
                    context.SaveChanges();
                }

                if (guildId == 0)
                {
                    try
                    {
                        user = context.Users.SingleOrDefault(m => m.Id == Context.User.Id);
                    }
                    catch (Exception)
                    {
                        throw new UserException("No default guild/corp set", "You're a member of multiple servers and I'm not sure which one you want to look at");
                    }
                }
                else
                {
                    user = context.Users.SingleOrDefault(m => m.Id == Context.User.Id && m.GuildId == guildId);
                }

                if (user == null)
                {
                    // Auto-create this user!
                    user = new Users
                    {
                        Id = Context.User.Id,
                        LastSeen = DateTime.Now,
                        Nickname = "", //Context.User.Username ?? "",
                        UserName = Context.User.Username,
                        GuildId = guildId
                    };
                    context.Users.Add(user);

                    context.SaveChanges();
                }
            }

            return user;
        }

        #region Validation
        /// <summary>
        /// Validates the Context.User for specified role.  Sends Message if fail.
        /// </summary>
        /// <param name="role"></param>
        /// <returns>bool</returns>
        public async Task<bool> HasRole(string role)
        {
            var hasRole = ((SocketGuildUser)Context.User).Roles.Any(r => r.Name == role);
            if (hasRole == false)
                await ReplyAsync($"{Context.User.DisplayName()} does not have the role of '{role}' ...");

            return hasRole;
        }
        /// <summary>
        /// Validates Context.User sent comand from Guild Chat. Sends Message if fail.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsFromGuildChat()
        {
            var IsFromGuildChat = Context.Guild.Id != 0;
            if (IsFromGuildChat == false)
                throw new RequiresDiscordGuildException();

            return IsFromGuildChat;
        }

        public async Task<bool> IsAdmin()
        {
            var IsAdmin = ((SocketGuildUser)Context.User).GuildPermissions.Administrator;
            if (IsAdmin == false)
                await ReplyAsync($"{Context.User.DisplayName()} is not a discord admin ...");

            return IsAdmin;
        }
        #endregion
    }
}
