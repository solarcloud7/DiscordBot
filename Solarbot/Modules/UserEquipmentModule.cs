﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Managers;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Xml;

namespace Solarbot.Modules
{
    public class UserEquipmentModule : BaseSolarbotModule
    {
        public const ulong EC1_ID = 484898879804801045;
        public const ulong EC2_ID = 487282460732817410;

        [Command("t3")]
        [Summary("test3")]
        public async Task TestStats3()
        {
            //var test = Path.GetFileName("layout.svg");
            //var xml = new XmlDocument();
            //xml.Load("Content/layout.svg");
            //SvgElement myRootElement = SvgFactory.LoadFromXML(xml, xml.DocumentElement);
            
        }

        [Command("t")]
        [Summary("test")]
        public async Task TestStats()
        {
            await TestStats(Context.User as SocketGuildUser);
        }

        [Command("t")]
        [Summary("test")]
        public async Task TestStats(SocketGuildUser member)
        {

            using (var context = new BotContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == member.Id && x.GuildId == Context.Guild.Id);
                if (user == null)
                {
                    await ReplyAsync($"No record for {member.DisplayName()}");
                    return;
                }

                var mock = new UserModules {
                    Shipmentcomputer = 5,
                    Battleship = 4,
                    Transport = 4,
                    Miner = 5
                };

                var builder = new EmbedBuilder()
                {
                    Author = new EmbedAuthorBuilder()
                    {
                        Name = member.DisplayName(),
                        IconUrl = member.GetAvatarUrl()
                    },
                    Color = new Color().Random(),
                    Timestamp = DateTime.Now,
                    //Description = "description",
                    ThumbnailUrl = member.GetAvatarUrl(),
                    ImageUrl = "https://cdn.discordapp.com/attachments/439149831425097729/487720933843402767/bitmap.png",//member.GetAvatarUrl(),
                    Footer = new EmbedFooterBuilder
                    {
                        IconUrl = Context.Client.CurrentUser.GetAvatarUrl(),
                        Text = "Made with CloudBot",
                    },
                    Fields = new List<EmbedFieldBuilder>
                    {
                        StatEmbedManager.GetTrade(mock),
                        StatEmbedManager.GetMining(mock),
                        StatEmbedManager.GetWeapons(mock),
                        StatEmbedManager.GetShields(mock),
                        StatEmbedManager.GetSupport(mock),
                        StatEmbedManager.GetShips(mock),
                    }
                };

                var embed = builder.Build();

                await ReplyAsync("", false, embed);
            }
        }

        [Command("t2")]
        [Summary("test")]
        public async Task SaveEmojis()
        {

            using (var context = new BotContext())
            {
                foreach (var guild in Context.Client.Guilds
                                .Where(g => g.Id == EC1_ID ||
                                            g.Id == EC2_ID))
                {

                    if (guild.Emotes.Count < 1)
                        continue;

                    foreach (var e in guild.Emotes)
                    {
                        if (context.CustomEmoji.Any(x => x.Name == e.Name))
                            continue;

                        if (e.Animated)
                            continue;

                        context.CustomEmoji.Add(new CustomEmoji
                        {
                            GuildName = guild.Name,
                            Name = e.Name.FirstLetterToUpperCase(),
                            Code = $"<{(e.Animated ? "a" : "")}:{e.Name}:{e.Id}>",
                            IsAnimated = e.Animated
                        });
                    }

                }
                context.SaveChanges();
            }
            await ReplyAsync("Saved Emoji's to DB");
        }

        [Command("EmojiList")]
        [Summary("EmojiList")]
        [Alias("EL")]
        public async Task TestEmojis()
        {
            var chunksize = 20;

            foreach (var g in Context.Client.Guilds
                        .Where(g => g.Id == EC1_ID ||
                                    g.Id == EC2_ID))
            {
                var emotes = g.Emotes.ToList();

                if (emotes.Count < 1)
                    continue;

                while (emotes.Count() > chunksize)
                {
                    var chunk = emotes.Take(chunksize).ToList();
                    emotes = emotes.Except(chunk).ToList();
                    await ReplyAsync(EmojiTable(chunk));
                }

                await ReplyAsync(EmojiTable(emotes));
            }
        }

        public string EmojiTable(List<GuildEmote> list)
        {
            var endList = "";
            list.ForEach(x => endList = endList + $"<{(x.Animated ? "a" : "")}:{x.Name}:{x.Id}>  =  `<{(x.Animated ? "a" : "")}:{x.Name}:{x.Id}>` \n");
            return endList;
        }

    }


    public class CustomEmbed
    {
        public Embed Embed()
        {
            var builder = new EmbedBuilder()
            .WithTitle("title ~~(did you know you can have markdown here too?)~~")
            .WithDescription("this supports [named links](https://discordapp.com) on top of the previously shown subset of markdown. ```\nyes, even code blocks```")
            .WithUrl("https://discordapp.com")
            .WithColor(new Color(0x52529B))
            .WithTimestamp(DateTimeOffset.FromUnixTimeMilliseconds(1536219002665))
            .WithFooter(footer =>
            {
                footer
                    .WithText("footer text")
                    .WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
            })
            .WithThumbnailUrl("https://cdn.discordapp.com/embed/avatars/0.png")
            .WithImageUrl("https://cdn.discordapp.com/embed/avatars/0.png")
            .WithAuthor(author =>
            {
                author
                    .WithName("author name")
                    .WithUrl("https://discordapp.com")
                    .WithIconUrl("https://cdn.discordapp.com/embed/avatars/0.png");
            })
            .AddField("🤔", "some of these properties have certain limits...")
            .AddField("😱", "try exceeding some of them!")
            .AddField("🙄", "an informative error should show up, and this view will remain as-is until all issues are fixed")
            .AddField("<:thonkang:219069250692841473>", "these last two", true)
            .AddField("<:thonkang:219069250692841473>", "are inline fields", true);

            return builder.Build();

            //await Context.Channel.SendMessageAsync(
            //    "this `supports` __a__ **subset** *of* ~~markdown~~ 😃 ```js\nfunction foo(bar) {\n  console.log(bar);\n}\n\nfoo(1);```",
            //    embed: embed)
            //    .ConfigureAwait(false);
        }
    }

}
