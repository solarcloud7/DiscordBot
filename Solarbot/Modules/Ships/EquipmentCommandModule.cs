﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solarbot.Exceptions;
using Solarbot.Models;

namespace Solarbot.Modules
{
    /**
     * Provide support for interfacing with what modules a given player has equipped to their ships.
     */
    public class EquipmentCommandModule : BaseSolarbotModule
    {
        [Command("equipment")]
        [Alias("equip")]
        [Remarks("Displays what equipment you'll be bringing to the WS")]
        public async Task ListMemberEquipment()
        {
            Users user = this.GetUserFromContext();
            List<BaseShips> ships;
            int numOfMods = 1;
            int c;
            var columns = new List<string>();

            // NOW, that we have the user details out of the way, we can poll for the actual Modules the user has set!
            ships = user.GetShips();

            if (ships.Count == 0)
            {
                await ReplyAsync("You have no ships set yet!  Go ahead and use !setship to save your ship levels.");
                return;
            }
            else
            {
                // Retrieve the most amount of modules any ship has; that will be used to calculate how many columns to display.
                foreach (BaseShips ship in ships)
                {
                    c = ship.GetEquippedModules().Count;
                    if (c > numOfMods)
                    {
                        numOfMods = c;
                    }
                }

                columns.Add("Ship Type");
                for (int i = 1; i <= numOfMods; i++)
                {
                    columns.Add("Module " + i.ToString());
                }

                var table = new Table(columns.ToArray());

                foreach (BaseShips ship in ships)
                {
                    var cells = new List<string>();
                    int i = 0;

                    cells.Add(ship.GetName());
                    foreach (EquipmentModules mod in ship.GetEquippedModules())
                    {
                        i++;
                        cells.Add(mod.GetShortName());
                    }

                    // Add empty columns to make everything match up.
                    while (i < numOfMods)
                    {
                        i++;
                        cells.Add("");
                    }

                    table.AddRow(cells.ToArray());
                }

                await ReplyAsync(table.ToString());
            }
        }

        [Command("setequipment")]
        [Alias("setequip")]
        [Remarks("Set what equipment you will be bringing on which ship, ex: `!setequipment battle dual passive emp tele`")]
        public async Task SetMemberEquipment(string shipname, params string[] modules)
        {
            Users user = this.GetUserFromContext();
            BaseShips ship = user.GetShip(shipname);

            ship.SetModules(modules);

            await ReplyAsync("Updated modules for your " + ship.GetName() + "!  Don't believe me?  Check out `!ships`");
        }


        [Command("clrequipment")]
        [Alias("clrequip")]
        [Remarks("clear your support equipment: `!clrequipment battle`")]
        public async Task ClearMemberEquipment(string shipname)
        {
            Users user = this.GetUserFromContext();
            BaseShips ship = user.GetShip(shipname);

            ship.ClearModules();

            await ReplyAsync("Updated modules for your " + ship.GetName() + "!  Don't believe me?  Check out `!ships`");
        }
    }
}
