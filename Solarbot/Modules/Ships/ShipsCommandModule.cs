﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solarbot.Exceptions;
using Solarbot.Models;
using Microsoft.EntityFrameworkCore;

namespace Solarbot.Modules
{
    /**
     * Provide support for interfacing with player module levels, such as viewing and updating.
     */
    public class ShipsCommandModule : BaseSolarbotModule
    {
        [Command("ships")]
        [Remarks("Displays your ship levels")]
        public async Task ListMemberShips()
        {
            Users user = this.GetUserFromContext();
            ShipTable table = new ShipTable();

            // NOW, that we have the user details out of the way, we can poll for the actual Modules the user has set!
            table.AddShips(user.GetShips());

            // Format the table a bit.
            table.ShowType();
            table.ShowLevel();
            table.ShowInfo();
            table.ShowWeapon();
            table.ShowShield();
            table.ShowModules();

            foreach (string chunk in table.GetTable().AsStringChunks())
            {
                await ReplyAsync(chunk);
            }
        }


        [Command("setship")]
        [Remarks("Sets a level of your ships, the first argument is the name of the ship type and the second is the level.  Example: `!setship transport 3`")]
        public async Task SetMemberModules(string shipname, uint level)
        {
            Users user = this.GetUserFromContext();
            BaseShips ship = user.GetShip(shipname);
            uint curLevel = ship.Level;

            if (level == curLevel)
            {
                await ReplyAsync(ship.GetName() + " is already at level " + curLevel + ", no changes performed");
            }
            else
            {
                ship.SetLevel(level);
                await ReplyAsync("Updated " + ship.GetName() + " from level " + curLevel + " to " + level + "!");
            }
        }


        [Command("guildships")]
        [Remarks("Display a list of [battleship,transport,miner] Guild ships that have been registered.  Ex: `!guildships b` Display a list of battleships.")]
        public async Task ListGuildShips(string type)
        {
            Users user = this.GetUserFromContext();
            List<BaseShips> ships = new List<BaseShips>();
            ShipTable table = new ShipTable();
            string shipType = BaseShips.ResolveName(type);

            using (var context = new BotContext())
            {
                var members = context.Users
                     .Where(p => p.GuildId == user.GuildId)
                     .ToList();

                foreach (var m in members)
                {
                    table.AddShip(m.GetShip(type));
                }
            }

            // Format the table a bit.
            table.ShowInfo();
            table.ShowOwner();

            if (shipType == "battleship")
            {
                table.ShowWeapon();
                table.ShowShield();
            }
            table.ShowModules();

            foreach (string chunk in table.GetTable().AsStringChunks())
            {
                await ReplyAsync(chunk);
            }
        }

        [Command("wsships")]
        [Remarks("Display a list of [battleship,transport,miner] Guild ships that are enrolled for white star.  Ex: `!wsships b` Display a list of battleships.")]
        public async Task ListGuildShipsWhiteStar(string type)
        {
            Users user = this.GetUserFromContext();
            List<BaseShips> ships = new List<BaseShips>();
            ShipTable table = new ShipTable();
            string shipType = BaseShips.ResolveName(type);

            using (var context = new BotContext())
            {
                var members = context.Users
                     .Where(p => p.GuildId == user.GuildId)
                     .Include(c => c.WhiteStarParticipant)
                     .Where(p => p.WhiteStarParticipant != null && p.WhiteStarParticipant.GuildId == user.GuildId)
                     .ToList();

                foreach (var m in members)
                {
                    table.AddShip(m.GetShip(type));
                }
            }

            // Format the table a bit.
            table.ShowInfo();
            table.ShowOwner();

            if (shipType == "battleship")
            {
                table.ShowWeapon();
                table.ShowShield();
            }
            table.ShowModules();

            foreach (string chunk in table.GetTable().AsStringChunks())
            {
                await ReplyAsync(chunk);
            }
        }
    }
}
