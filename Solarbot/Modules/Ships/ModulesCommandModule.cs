﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solarbot.Exceptions;
using Solarbot.Models;

namespace Solarbot.Modules
{
    /**
     * Provide support for interfacing with player module levels, such as viewing and updating.
     */
    public class ModulesCommandModule : BaseSolarbotModule
    {
        [Command("modules")]
        [Remarks("Displays your module levels")]
        public async Task ListMemberModules()
        {
            Users user = this.GetUserFromContext();
            List<EquipmentModules> mods;

            // NOW, that we have the user details out of the way, we can poll for the actual Modules the user has set!
            mods = user.GetEquipmentModules();

            if (mods.Count == 0)
            {
                await ReplyAsync("You have no modules set yet!  Go ahead and use (something) to save your module levels.");
                return;
            }
            else
            {
                var table = new Table("Module", "Level", "Info", "code");

                foreach (var m in mods)
                {
                    table.AddRow(m.GetName(), m.Level.ToString(), m.GetInfo(), m.GetShortName());
                }

                foreach (string chunk in table.AsStringChunks())
                {
                    await ReplyAsync(chunk);
                }
            }
        }

        [Command("setmodule")]
        [Remarks("Sets a level of one of your modules, the first argument is the name of the module and the second is the level.  Example: `!setmodule alphashield 3`")]
        public async Task SetMemberModules(string modname, uint level)
        {
            Users user = this.GetUserFromContext();
            EquipmentModules mod = user.GetEquipmentModule(modname);
            uint curLevel = mod.Level;

            if (level == curLevel)
            {
                await ReplyAsync(mod.GetName() + " is already at level " + curLevel + ", no changes performed");
            }
            else
            {
                mod.SetLevel(level);
                await ReplyAsync("Updated " + mod.GetName() + " from level " + curLevel + " to " + level + "!");
            }
        }
    }
}
