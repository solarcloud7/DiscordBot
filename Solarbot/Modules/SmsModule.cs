﻿using Discord;
using Discord.Commands;
using Solarbot.Data;
using Solarbot.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class SmsModule : ModuleBase<SocketCommandContext>
    {
        [Command("setsmsrole")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task SmsSetRole(string roleName)
        {
            var guildId = Context.Guild.Id;
            if (guildId == 0)
            {
                throw new RequiresDiscordGuildException();
            }

            try
            {
                using (var context = new BotContext())
                {

                    //context.GuildSettings.FirstOrDefault(g => g.GuildId == guildId).SmsRequiredRole = roleName;
                    //context.SaveChanges();

                    await ReplyAsync($"The required role to send SMS Messages has been changed by <@{Context.User.Id}> to {roleName}!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Command("registernumber")]
        [Alias("rn")]
        public async Task RegisterNumer(int phoneNumber)
        {
            //Must be from DM to keep number Private!
            if (Context.Guild.Id != 0)
            {
                var privateChannel = await Context.User.GetOrCreateDMChannelAsync();
                await privateChannel.SendMessageAsync("Please keep your phone number private. Use '!rn [phoneNumber]' from this direct message!");
                await Context.Message.DeleteAsync();
            }

            try
            {
                //TODO finish registering numbers

                //using (var context = new BotContext())
                //{

                //    context.Guilds.FirstOrDefault(g => g.GuildId == guildId).SmsRequiredRole = roleName;
                //    context.SaveChanges();

                //    await ReplyAsync($"The required role to send SMS Messages has been changed by <@{Context.User.Id}> to {roleName}!");
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
