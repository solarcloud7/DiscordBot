﻿using ClosedXML.Excel;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class Reports : BaseSolarbotModule
    {

        public const string TempFileName = "temp.xlsx";
        public const string TempRoleFileName = "DiscordRoles.xlsx";

        [Command("CommandReport")]
        [Summary("Lists all commands")]
        public async Task CommandReport()
        {
            foreach (var module in Program.Commands.GroupBy(x => x.Module))
            {
                var table = new Table("Name", "Alias", "Params", "Summary");
                var moduleName = module.Key.Name;

                foreach (var cmd in module)
                {
                    var name = "!" + cmd.Aliases.FirstOrDefault();
                    var alias = cmd.Aliases.Count() > 1 ? "!" + cmd.Aliases.ElementAt(1) : "";
                    var summary = cmd.Summary ?? "";
                    List<string> paramList = new List<string>();
                    cmd.Parameters.ForEach(x => paramList.Add(x.Name));
                    var param = string.Join(",", paramList);

                    table.AddRow(name, alias, param, summary);
                }

                foreach (var chunk in table.AsStringChunks(moduleName))
                {
                    await ReplyAsync(chunk);
                }
            }

        }

        [Command("report")]
        [Alias("r")]
        public async Task Report()
        {
            using (var context = new BotContext())
            {
                //validation
                if (!IsFromGuildChat() &&
                    !await IsAdmin())
                    return;

                var roleList = context.GuildSettings
                   .Where(g => g.GuildId == Context.Guild.Id &&
                               g.SettingId == (int)Setting.WsWhitelistRole)
                   .Select(g => g.Value)
                   .ToList();

                if (roleList.Count() == 0)
                {
                    await ReplyAsync("use !wsrole [role name] to set specific role that can join white stars.");
                    return;
                }

                List<SocketGuildUser> members = new List<SocketGuildUser>();
                foreach (var role in Context.Guild.Roles.Where(r => roleList.Any(rl => rl == r.Name)))
                {
                    members.AddRange(role.Members.ToList());
                }

                var data = context.Users
                            .Include(u => u.MemberStats)
                            .Where(u => u.GuildId == Context.Guild.Id &&
                                        members.Any(m => m.Id == u.Id))
                            .Select(u => new
                            {
                                Name = u.DisplayName,
                                Level = (u.MemberStats.Count > 0) ?
                                             u.MemberStats.Max(s => s.Level) : 0,
                                Influence = (u.MemberStats.Count > 0) ?
                                             u.MemberStats.Max(s => s.Influence) : 0,
                                TransportLevel = u.ShipTransport,
                                CargoModLevel = u.ModuleTradeCargo,
                                GenesisLevel = u.ModuleMiningGenesis,
                            });

                EntityToExcelSheet("Report", data, "Blank");

                await Context.Channel.SendFileAsync("Report.xlsx");
            }

        }
        [Command("reportTest")]
        [Alias("rt")]
        public async Task ReportTest()
        {

            var url = "https://1drv.ms/x/s!Al16DFlXe-TAm1-FM-L5zkX5JklS";

            GetFile(url);

            var workbook = new XLWorkbook(TempFileName);

            var ws = workbook.Worksheet(1);

            var rowUsed = ws.RowsUsed().Count();
            var colUsed = ws.ColumnsUsed().Count();

            //Headers
            List<string> headers = new List<string>();
            foreach (var col in ws.ColumnsUsed())
            {
                headers.Add(col.Cell(1).Value.ToString().Truncate(5));
            }
            var table = new Solarbot.Utilities.Table(headers.ToArray());

            for (int r = 2; r <= rowUsed; r++)
            {
                List<string> rowData = new List<string>();
                foreach (var col in ws.ColumnsUsed())
                {
                    rowData.Add(col.Cell(r).Value.ToString());
                }
                table.AddRow(rowData.ToArray());
            }

            foreach (var chunk in table.AsStringChunks())
            {
                await ReplyAsync(chunk);
            }
        }


        [Command("reportadd")]
        [Alias("ra")]
        public async Task ReportAdd()
        {
            if (Context.Message.Attachments.Count != 1)
            {
                await ReplyAsync($"Need 1 parameter.  Recieved {Context.Message.Attachments.Count}");
                return;
            }

            var atchmnt = Context.Message.Attachments.First();
            if (!GetFile(atchmnt.Url))
            {
                await ReplyAsync($"Failed to grab {atchmnt.Url}");
                return;
            }

            //var url = "https://cdn.discordapp.com/attachments/441028018375360523/482660653572554752/Report_2.xlsx";
            //GetFile(url);

            var workbook = new XLWorkbook(TempFileName);

            var ws = workbook.Worksheet(1);

            var rowUsed = ws.RowsUsed().Count();
            var colUsed = ws.ColumnsUsed().Count();

            //Headers
            List<string> headers = new List<string>();
            foreach (var col in ws.ColumnsUsed())
            {
                headers.Add(col.Cell(1).Value.ToString().Truncate(5));
            }
            var table = new Solarbot.Utilities.Table(headers.ToArray());

            for (int r = 2; r <= rowUsed; r++)
            {
                List<string> rowData = new List<string>();
                foreach (var col in ws.ColumnsUsed())
                {
                    rowData.Add(col.Cell(r).Value.ToString());
                }
                table.AddRow(rowData.ToArray());
            }

            foreach (var chunk in table.AsStringChunks())
            {
                await ReplyAsync(chunk);
            }
        }

        //[Command("reportroles")]
        //[Alias("rr")]
        //public async Task RolesReport()
        //{
        //    //validation
        //    if (!IsFromGuildChat() &&
        //        !await IsAdmin())
        //        return;

        //    var roles = Context.Guild.Roles;
        //    var users = Context.Guild.Users;

        //    using (var workbook = new XLWorkbook())
        //    {
        //        var worksheet = workbook.Worksheets.Add("Roles");

        //        var validSheet = workbook.Worksheets.Add("Validation");
        //        validSheet.Cell("A1").Value = "TRUE";
        //        validSheet.Cell("A2").Value = "FALSE";

        //        // column headings
        //        for (var i = 0; i < roles.Count() + 1; i++)
        //        {
        //            if (i == 0)
        //                worksheet.Cell(1, 1).Value = "Name";
        //            else
        //                worksheet.Cell(1, i + 1).Value = roles.ElementAt(i - 1).Name;
        //        }

        //        for (int r = 2; r < users.Count + 2; r++)
        //        {
        //            for (int c = 1; c < roles.Count + 2; c++)
        //            {
        //                var user = users.ElementAt(r - 2);

        //                if (c == 1)
        //                    worksheet.Cell(r, 1).Value = user.DisplayName();
        //                else
        //                {
        //                    var cell = worksheet.Cell(r, c);
        //                    var value = roles.ElementAt(c - 2).Members.Any(u => u.Id == user.Id);
        //                    cell.Value = value;
        //                    cell.Style.Fill.BackgroundColor = value ?
        //                        XLColor.FromTheme(XLThemeColor.Accent3, .1) :
        //                        XLColor.FromTheme(XLThemeColor.Accent2, .1);
        //                    cell.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
        //                    cell.DataValidation.List(validSheet.Range("A1:A2"));
        //                }

        //            }
        //        }

        //        worksheet.Columns().AdjustToContents();
        //        worksheet.SetAutoFilter(true);
        //        workbook.SaveAs(TempRoleFileName);

        //        await Context.Channel.SendFileAsync(TempRoleFileName);
        //    }

        //}

        public static bool GetFile(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();


                // Check that the remote file was found. The ContentType
                // check is performed since a request for a non-existent
                // image file might be redirected to a 404-page, which would
                // yield the StatusCode "OK", even though the image was not
                // found.
                if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect))
                {

                    // if the remote file was found, download it
                    using (Stream inputStream = response.GetResponseStream())
                    {
                        var file = File.Create($"{Directory.GetCurrentDirectory()}\\{TempFileName}");
                        inputStream.CopyTo(file);
                        file.Close();
                    }

                    return true;
                }
                else
                {
                    Console.WriteLine("Failed Response");
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "          " + url);
                return false;
            }
        }

        public void EntityToExcelSheet<T>(string fileName, IEnumerable<T> result, string sheetName = "Sheet1")
        {
            //using (var workbook = new XLWorkbook())
            //{
            //    var worksheet = workbook.Worksheets.Add(sheetName);

            //    var dt = CreateDataTable(result);

            //    // column headings
            //    for (var i = 0; i < dt.Columns.Count; i++)
            //    {
            //        worksheet.Cell(1, i + 1).Value = dt.Columns[i].ColumnName;
            //    }

            //    // rows
            //    for (var i = 0; i < dt.Rows.Count; i++)
            //    {
            //        // to do: format datetime values before printing
            //        for (var j = 0; j < dt.Columns.Count; j++)
            //        {
            //            worksheet.Cell(i + 2, j + 1).Value = dt.Rows[i][j];
            //        }
            //    }

            //    worksheet.Columns().AdjustToContents();

            //    workbook.SaveAs($"{fileName}.xlsx");
            //}
        }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }
}
