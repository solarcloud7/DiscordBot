﻿using Discord.Commands;
using Solarbot.Utilities;
using System.Linq;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class NetworkInfo : ModuleBase<SocketCommandContext>
    {
        [Command("network")]
        [Alias("net")]
        public async Task Network()
        {
            var table = new Table("Name", "Owner", "Users");
            foreach (var guild in Context.Client.Guilds)
            {
                table.AddRow(guild.Name, guild.Owner.Username, guild.Users.Count().ToString());

                var channel = guild.DefaultChannel ?? guild.TextChannels.FirstOrDefault();
                if (channel != null)
                {

                }
            }

            await ReplyAsync(table.ToString());

            await Context.Message.DeleteAsync();
        }


        

    }

}
