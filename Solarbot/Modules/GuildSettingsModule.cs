﻿using Discord;
using Discord.Commands;
using Solarbot.Data;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class GuildSettingsModule : BaseSolarbotModule
    {


        #region White Star Roles
        [Command("wsroleadd")]
        [Alias("wsra")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsAddRole(string roleName)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var model = new WsWhitelistRole
            {
                WhitelistRole = roleName
            };

            var message = AddSetting(Setting.WsWhitelistRole, model.Serialize());

            await ReplyAsync(message);
        }

        [Command("wsroleremove")]
        [Alias("wsrr")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsRemoveRole(string roleName)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var exisiting = context.GuildSettings
                    .Where(gs => gs.GuildId == Context.Guild.Id)
                    .FirstOrDefault(gs => WsWhitelistRole.DeserializeObject(gs.Value).WhitelistRole == roleName);
                if (exisiting == null)
                {
                    await ReplyAsync($"'There does a Whitelist role '{roleName}'. Use ``` !guildsettings``` to see your roles");
                    return;
                }

                var message = RemoveSetting(Setting.WsWhitelistRole, exisiting.Value);
                await ReplyAsync(message);
            }
        }

        [Command("wsblacklistroleadd")]
        [Alias("wsbra")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsBlacklistAddRole(string roleName)
        {
            //validation
            if (!IsFromGuildChat())
                return;
            var model = new WsBlacklistRole
            {
                BlacklistRole = roleName
            };

            var message = AddSetting(Setting.WsBlacklistRole, model.Serialize());

            await ReplyAsync(message);
        }

        [Command("wsBlacklistroleremove")]
        [Alias("wsbrr")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsBlacklistRemoveRole(string roleName)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var exisiting = context.GuildSettings
                    .Where(gs => gs.GuildId == Context.Guild.Id)
                    .FirstOrDefault(gs => WsBlacklistRole.DeserializeObject(gs.Value).BlacklistRole == roleName);
                if (exisiting == null)
                {
                    await ReplyAsync($"'There does a Blacklist role '{roleName}'. Use ``` !guildsettings``` to see your roles");
                    return;
                }

                var message = RemoveSetting(Setting.WsBlacklistRole, exisiting.Value);
                await ReplyAsync(message);
            }

        }
        #endregion

        #region White Star Alias
        [Command("WsAliasAdd")]
        [Alias("wsa")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsAddAlias(int index, string name)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var model = new WhiteStarAlias
            {
                Index = index,
                Alias = name
            };

            var message = AddSetting(Setting.WhiteStarIndexAlias, model.Serialize());

            await ReplyAsync(message);
        }

        [Command("WsAliasRemove")]
        [Alias("wsr")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsRemoveAlias(int index)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var exisiting = context.GuildSettings
                    .Where(gs => gs.GuildId == Context.Guild.Id)
                    .FirstOrDefault(gs => WhiteStarAlias.DeserializeObject(gs.Value).Index == index);
                if (exisiting == null)
                {
                    await ReplyAsync($"'There does not exist an alias for index #{index}. Use ``` !guildsettings``` to see your roles");
                    return;
                }

                var message = RemoveSetting(Setting.WhiteStarIndexAlias, exisiting.Value);
                await ReplyAsync(message);
            }

        }
        #endregion

        #region Settings CRUD
        [Command("GuildSettings")]
        [Alias("GSettings")]
        public async Task GuildSettings()
        {
            using (var context = new BotContext())
            {
                var rolesGroups = context.GuildSettings
                                        .Where(s => s.GuildId == Context.Guild.Id)
                                        .Join(context.Settings,
                                            gs => gs.SettingId,
                                            s => s.SettingId,
                                            (gs, s) => new { GuildSetting = gs.GetBaseSetting(), Settings = s })
                                        .GroupBy(g => g.Settings.SettingId)
                                        .ToList();
               
                foreach (var g in rolesGroups)
                {          
                    var description = g.FirstOrDefault().Settings.Description;
                    IBaseSetting baseSetting = g.FirstOrDefault().GuildSetting;
                    var message = $"***{description}.***";

                    List<string> headings = new List<string> { "Id" };
                    headings.AddRange(baseSetting.Properties());
                    var table = new Table(headings.ToArray());

                    var index = 0;
                    foreach (var s in g)
                    {
                        index++;
                        List<string> list = new List<string>{ index.ToString()};
                        list.AddRange(s.GuildSetting.Values());
                        table.AddRow(list.ToArray());

                    }
                    foreach (var chunk in table.AsStringChunks(message))
                    {
                        await ReplyAsync(chunk);
                    }
                }
            }
        }


        public string AddSetting(Setting type, string value)
        {
            using (var context = new BotContext())
            {
                var settingType = context.Settings.FirstOrDefault(s => s.SettingId == (int)type);

                var guildSetting = context.GuildSettings
                    .FirstOrDefault(gs => gs.GuildId == Context.Guild.Id &&
                                gs.Value == value &&
                                gs.SettingId == (int)type);

                if (guildSetting != null)
                {
                    return $"'{value}' is already a {settingType.Description}.";
                }

                context.GuildSettings.Add(new GuildSettings
                {
                    SettingId = (int)type,
                    Value = value,
                    GuildId = Context.Guild.Id,
                    CreatedDate = DateTime.Now,
                    Name = Context.Guild.Name,
                    CreatedId = Context.User.Id
                });
                context.SaveChanges();

                return $"<@{Context.User.Id}> added a new {settingType.Description}!";
            }
        }


        public string RemoveSetting(Setting type, string value)
        {
            using (var context = new BotContext())
            {
                var settingType = context.Settings.FirstOrDefault(s => s.SettingId == (int)type);

                var guildSetting = context.GuildSettings
                    .FirstOrDefault(gs => gs.GuildId == Context.Guild.Id &&
                                gs.Value == value &&
                                gs.SettingId == (int)type);

                if (guildSetting == null)
                {
                    return $"'{value}' does not exist as {settingType.Description}. Use ``` !guildsettings``` to see your roles";
                }

                context.GuildSettings.Remove(guildSetting);
                context.SaveChanges();

                return $"<@{Context.User.Id}> removed '{value}'as a {settingType.Description}!";
            }
        }
        #endregion
    }
}
