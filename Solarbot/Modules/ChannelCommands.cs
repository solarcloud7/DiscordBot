﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class ChannelCommands : ModuleBase<SocketCommandContext>
    {

        private static Dictionary<ulong, ClearCasheModel> MessageCache = new Dictionary<ulong, ClearCasheModel>();


        [Command("clr")]
        [Summary("deletes set amount of text")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task ClrAsync(int amount = 100)
        {
            try
            {
                if (Context.IsPrivate)
                {
                    await ReplyAsync("Cant call command from a direct message");
                    return;
                }

                var guildUser = Context.Guild.GetUser(Context.Client.CurrentUser.Id);
                var chnl = Context.Message.Channel as SocketGuildChannel;

                var roles = guildUser.Roles.Any(r => r.Name == "");

                if (guildUser.GetPermissions(chnl).ManageChannel)
                {
                    var messages = await Context.Channel.GetMessagesAsync(amount)
                                            .FlattenAsync();

                    //get only for past 2 weeks
                    //messages = messages.Where(m => m.CreatedAt > DateTime.Now.AddDays(-14));

                    var cache = new ClearCasheModel
                    {
                        CreatedUserId = guildUser.Id,
                        GuildId = Context.Guild.Id,
                        ChannelId = Context.Channel.Id,
                        Messages = messages
                    };

                    MessageCache[Context.Guild.Id] = cache;

                    await ReplyAsync($"**!clr** ['**yes**', '**yup**',  or '**confirm**'] to confirm clearing {messages.Count()} from {cache.GetAuthor}.");

                }
                else
                {
                    await ReplyAsync("No Permission to Manage Messages");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("clr")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task ClrAsync(SocketUser user, int amount = 100)
        {
            try
            {
                if (Context.IsPrivate)
                {
                    await ReplyAsync("Cant call command from a direct message");
                    return;
                }

                var guildUser = Context.Guild.GetUser(Context.Client.CurrentUser.Id);
                var chnl = Context.Message.Channel as SocketGuildChannel;

                if (guildUser.GetPermissions(chnl).ManageChannel)
                {
                    var messages = await Context.Channel.GetMessagesAsync(1000).FlattenAsync();

                    //get only for past 2 weeks
                    messages = messages.Where(m => m.CreatedAt > DateTime.Now.AddDays(-14));

                    var cache = new ClearCasheModel
                    {
                        CreatedUserId = guildUser.Id,
                        GuildId = Context.Guild.Id,
                        ChannelId = Context.Channel.Id,
                        Author = user.Username,
                        Messages = messages.Where(m => m.Author.Id == user.Id).Take(amount)
                    };

                    MessageCache[Context.Guild.Id] = cache;

                    await ReplyAsync($"!clr ['yes', 'yup',  or 'confirm'] to confirm clearing {cache.Messages.Count()} from {cache.GetAuthor}.");
                }
                else
                {
                    await ReplyAsync("No Permission to Manage Messages");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("clr")]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task ClrAsync(string text)
        {
            try
            {
                //No Private Message
                if (Context.IsPrivate)
                {
                    await ReplyAsync("Cant call command from a direct message");
                    return;
                }

                var guildUser = Context.Guild.GetUser(Context.Client.CurrentUser.Id);
                var chnl = Context.Message.Channel as SocketGuildChannel;

                //Must Manage Chanel
                if (!guildUser.GetPermissions(chnl).ManageChannel)
                {
                    await ReplyAsync("No Permission to Manage Messages");
                    return;
                }

                var cache = MessageCache[Context.Guild.Id];

                //Must Be Same User
                if (cache.CreatedUserId != guildUser.Id)
                {
                    await ReplyAsync("Same user is required to confirm the command");
                    return;
                }

                //Must Be Same Channel
                if (cache.ChannelId != Context.Channel.Id)
                {
                    await ReplyAsync("Same channel is required to confirm the command");
                    return;
                }

                var count = cache.Messages.Count();

                //Must match text to confirm
                if (string.Equals(text, "Yes", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(text, "Yup", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(text, "Confirm", StringComparison.OrdinalIgnoreCase))
                {
                    await (Context.Channel as SocketTextChannel).DeleteMessagesAsync(cache.Messages);
                    
                    MessageCache[Context.Guild.Id] = null;

                    await Context.Message.DeleteAsync();
                }
                else
                {
                    await ReplyAsync("!clr ['yes', 'yup',  or 'confirm'] to confirm clearing");
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public class ClearCasheModel
        {
            public ulong GuildId { get; set; }
            public ulong ChannelId { get; set; }
            public ulong CreatedUserId { get; set; }
            public string Author { get; set; }
            public IEnumerable<IMessage> Messages { get; set; }

            //helpers
            public string GetAuthor
            {
                get { return (string.IsNullOrEmpty(this.Author)) ? "everyone" : this.Author; }
            }
        }
    }
}
