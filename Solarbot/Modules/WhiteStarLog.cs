﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class WsAccept : BaseSolarbotModule
    {

        #region Join

        public string JoinWhiteStar(ulong userId, ulong whiteStarIndex, string alias = "")
        {
            //Get Role
            using (var context = new BotContext())
            {
                //whitelist
                var whitelistRoles = context.GuildSettings
                    .Where(g => g.GuildId == Context.Guild.Id &&
                                g.SettingId == (int)Setting.WsWhitelistRole)
                    .Select(g => WsWhitelistRole.DeserializeObject(g.Value).WhitelistRole)
                    .ToList();

                //blacklist
                var blacklistRoles = context.GuildSettings
                    .Where(g => g.GuildId == Context.Guild.Id &&
                                g.SettingId == (int)Setting.WsBlacklistRole)
                    .Select(g => WsBlacklistRole.DeserializeObject(g.Value).BlacklistRole)
                    .ToList();

                //guild must set a role
                if (whitelistRoles.Count() == 0)
                    return "use !wsrole [role name] to set specific role that can join white stars.";

                //check has whitelist
                if (!Context.Guild.GetUser(userId).Roles.Any(r => whitelistRoles.Any(x => x == r.Name)))
                    return $"<@{userId}> is not a member of any of the accepted roles yet... \n !GuildSettings shows acceptable roles.";

                //abort if in blacklist
                var blistrole = Context.Guild.GetUser(userId).Roles.Where(r => blacklistRoles.Any(x => x == r.Name)).ToList();
                if (blistrole.Count() > 0)
                {
                    return $"<@{userId}> is in a blacklisted role... \n { String.Join(", ", blistrole)}";
                }

                //check if existing in a whitestar log
                var record = context.WhiteStarParticipants.FirstOrDefault(x => x.UserId == userId &&
                                                                                x.GuildId == Context.Guild.Id);

                if (record == null)
                {
                    //create
                    context.WhiteStarParticipants.Add(new WhiteStarParticipants
                    {
                        UserId = userId,
                        AcceptDate = DateTime.Now,
                        UserName = Context.Guild.GetUser(userId).DisplayName(),
                        GuildId = Context.Guild.Id,
                        WhiteStarIndex = whiteStarIndex
                    });

                    context.SaveChanges();

                    var wsName = string.IsNullOrEmpty(alias) ? whiteStarIndex.ToString() : alias;
                    return $"<@{userId}> has enrolled for the next White Star #{wsName}!";
                }
                else
                {
                    //update
                    return $"<@{userId}> was already enrolled in White Star List #{record.WhiteStarIndex} {alias}, {record.AcceptDate.DisplayTime()}!";
                }

            }
        }

        [Command("join")]
        [Alias("j", "jim")]
        public async Task WsJoinAsync(SocketGuildUser user, ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var message = JoinWhiteStar(user.Id, whiteStarIndex);

            await ReplyAsync(message);

            //await Context.Message.DeleteAsync();

        }

        [Command("join")]
        [Alias("j", "jim")]
        public async Task WsJoinAliasAsync(SocketGuildUser user, string alias)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var index = GetIndexFromAlias(alias);
                if (index == -1)
                {
                    await ReplyAsync($"There is no Alias for '{alias}'");
                    return;
                }

                var message = JoinWhiteStar(user.Id, (ulong)index, alias);

                await ReplyAsync(message);
            }
        }

        public int GetIndexFromAlias(string alias)
        {
            using (var context = new BotContext())
            {
                var guildSettings = context.GuildSettings
                    .Where(x => x.GuildId == Context.Guild.Id && x.SettingId == (int)Setting.WhiteStarIndexAlias)
                    .FirstOrDefault(x => string.Equals(WhiteStarAlias.DeserializeObject(x.Value).Alias, alias, StringComparison.OrdinalIgnoreCase));
                if (guildSettings == null)
                {
                    return -1;
                }
                return WhiteStarAlias.DeserializeObject(guildSettings.Value).Index;
            }
        }

        public string GetAliasFromIndex(int index)
        {
            using (var context = new BotContext())
            {
                var guildSettings = context.GuildSettings
                    .Where(x => x.GuildId == Context.Guild.Id && x.SettingId == (int)Setting.WhiteStarIndexAlias)
                    .FirstOrDefault(x => WhiteStarAlias.DeserializeObject(x.Value).Index == index);
                if (guildSettings == null)
                {
                    return "";
                }

                return WhiteStarAlias.DeserializeObject(guildSettings.Value).Alias;
            }
        }

        [Command("join")]
        [Alias("j", "jim")]
        public async Task WsJoinAsync(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var message = JoinWhiteStar(Context.User.Id, whiteStarIndex);

            await ReplyAsync(message);

            //await Context.Message.DeleteAsync();
        }

        [Command("join")]
        [Alias("j", "jim")]
        public async Task WsJoinAliasAsync(string alias)
        {
            //validation
            using (var context = new BotContext())
            {
                var index = GetIndexFromAlias(alias);
                if (index == -1)
                {
                    await ReplyAsync($"There is no Alias for '{alias}'");
                    return;
                }

                var message = JoinWhiteStar(Context.User.Id, (ulong)index, alias);

                await ReplyAsync(message);
            }
        }
        #endregion

        #region Who
        [Command("wswho1")]
        [Alias("ws1")]
        public async Task WsWho1Async(string alias)
        {
            var index = GetIndexFromAlias(alias);
            if (index == -1)
            {
                await ReplyAsync($"There is no Alias for '{alias}'");
                return;
            }

            await WsWho1Async((ulong)index);
        }


        [Command("wswho1")]
        [Alias("ws1")]
        public async Task WsWho1Async(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                //Search Members
                var members = context.Users
                    .Where(p => p.GuildId == Context.Guild.Id)
                    .Include(c => c.WhiteStarParticipant)
                    .Include(c => c.MemberStats)
                    .Where(p => p.WhiteStarParticipant != null &&
                                p.WhiteStarParticipant.GuildId == Context.Guild.Id &&
                                p.WhiteStarParticipant.WhiteStarIndex == whiteStarIndex)
                    .Select(m => new WhoModel
                    {
                        CurrentInfluence = m.MemberStats.CurrentInfluence(),
                        Nickname = m.Nickname,
                        UserName = m.UserName,
                        GuildId = m.GuildId
                    })
                    .ToList()
                    .OrderByDescending(m => m.CurrentInfluence);

                //Create Table
                var table = new Table("Order", "Name", "Influence");
                int i = 1;
                foreach (var m in members)
                {
                    var nick = (!string.IsNullOrEmpty(m.Nickname)) ? m.Nickname : m.UserName;
                    table.AddRow(i.ToString(), nick.FirstLetterToUpperCase(), m.CurrentInfluence.ToString());
                    i++;
                }

                var alias = GetAliasFromIndex((int)whiteStarIndex);
                var name = string.IsNullOrEmpty(alias) ? whiteStarIndex.ToString() : alias;
                await ReplyAsync($"***White Star #{name}" + table.ToString());
            }
        }

        [Command("wswho2")]
        [Alias("ws2")]
        public async Task WsWho2Async(string alias)
        {
            var index = GetIndexFromAlias(alias);
            if (index == -1)
            {
                await ReplyAsync($"There is no Alias for '{alias}'");
                return;
            }

            await WsWho2Async((ulong)index);
        }

        [Command("wswho2")]
        [Alias("ws2")]
        public async Task WsWho2Async(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                //Search Members
                var members = context.Users
                   .Where(p => p.GuildId == Context.Guild.Id)
                   .Include(c => c.WhiteStarParticipant)
                   .Include(c => c.MemberStats)
                   .Where(p => p.WhiteStarParticipant != null &&
                               p.WhiteStarParticipant.GuildId == Context.Guild.Id &&
                               p.WhiteStarParticipant.WhiteStarIndex == whiteStarIndex)
                   .Select(m => new WhoModel
                   {
                       CurrentInfluence = m.MemberStats.CurrentInfluence(),
                       Nickname = m.Nickname,
                       UserName = m.UserName,
                       AcceptedDate = m.WhiteStarParticipant.AcceptDate
                   })
                   .OrderBy(m => m.AcceptedDate)
                   .ToList();

                //Create Table
                var table = new Table("Order", "Name", "When");
                for (int i = 0; i < members.Count; i++)
                {
                    var nick = members[i].Nickname;
                    var name = members[i].UserName;
                    var displayName = (string.IsNullOrEmpty(nick)) ? name : nick;

                    table.AddRow((i + 1).ToString(), displayName.FirstLetterToUpperCase(), members[i].AcceptedDate.DisplayTime());
                }

                var alias = GetAliasFromIndex((int)whiteStarIndex);
                var wsName = string.IsNullOrEmpty(alias) ? whiteStarIndex.ToString() : alias;
                await ReplyAsync($"***White Star #{wsName}" + table.ToString());
            }
        }

        [Command("wswho3")]
        [Alias("ws3")]
        public async Task WsWho3Async(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                //whitelists
                var whitelistRoles = context.GuildSettings
                    .Where(g => g.GuildId == Context.Guild.Id &&
                                g.SettingId == (int)Setting.WsWhitelistRole)
                    .Select(g => g.Value)
                    .ToList();


                //blacklist
                var blacklistRoles = context.GuildSettings
                    .Where(g => g.GuildId == Context.Guild.Id &&
                                g.SettingId == (int)Setting.WsBlacklistRole)
                    .Select(g => g.Value)
                    .ToList();

                //must have a whitelisted role
                if (whitelistRoles.Count() == 0)
                {
                    await ReplyAsync("use !wsrole [role name] to set specific role that can join white stars.");
                    return;
                }

                //currently playing in this white star index
                var currentWS = context.Users
                   .Where(p => p.GuildId == Context.Guild.Id)
                   .Include(c => c.WhiteStarParticipant)
                   .Include(c => c.MemberStats)
                   .Where(p => p.WhiteStarParticipant != null &&
                               p.WhiteStarParticipant.GuildId == Context.Guild.Id &&
                               p.WhiteStarParticipant.WhiteStarIndex == whiteStarIndex)
                   .Select(m => new WhoModel
                   {
                       CurrentInfluence = m.MemberStats.CurrentInfluence(),
                       Nickname = m.Nickname,
                       UserName = m.UserName,
                       AcceptedDate = m.WhiteStarParticipant.AcceptDate,
                       UserID = m.Id
                   })
                   .OrderBy(m => m.AcceptedDate)
                   .ToList();

                //already playing in a white star on this guild
                var busy = context.WhiteStarParticipants
                            .Where(p => p.GuildId == Context.Guild.Id)
                            .ToList();


                List<SocketGuildUser> available = new List<SocketGuildUser>();
                //those that have permission to play
                foreach (var role in Context.Guild.Roles.Where(r => whitelistRoles.Any(rl => rl == r.Name)))
                {
                    available.AddRange(role.Members
                        .Where(a => !busy.Any(m => m.UserId == a.Id))
                        .Where(u => !blacklistRoles.Any(r => u.Roles.Any(x => x.Name == r)))
                        .ToList());
                }

                var table = new Table("Order", "Name");

                int index = 0;
                for (int i = 0; i < currentWS.Count; i++)
                {
                    var nick = currentWS[i].Nickname;
                    var name = currentWS[i].UserName;
                    var displayName = (string.IsNullOrEmpty(nick)) ? name : nick;

                    index++;
                    table.AddRow(index.ToString(), displayName.FirstLetterToUpperCase());
                }

                var table2 = new Table("Order", "Name");

                for (int i = 0; i < available.Count; i++)
                {
                    index++;
                    table2.AddRow(index.ToString(), available[i].DisplayName());
                }

                var alias = GetAliasFromIndex((int)whiteStarIndex);
                var wsName = string.IsNullOrEmpty(alias) ? whiteStarIndex.ToString() : alias;
                var display1 = $"Who **HAVE** joined White Star #{wsName}: \n" + table;
                await ReplyAsync(display1);

                var chunks = table2.AsStringChunks();
                for (var i = 0; i < chunks.Count(); i++)
                {
                    if (i == 0)
                        await ReplyAsync("Who **HAVE NOT** Joined: \n" + chunks[i]);
                    else
                        await ReplyAsync(chunks[i]);
                }
            }
        }

        [Command("wswho3")]
        [Alias("ws3")]
        public async Task WsWho3Async(string alias)
        {
            var index = GetIndexFromAlias(alias);
            if (index == -1)
            {
                await ReplyAsync($"There is no Alias for '{alias}'");
                return;
            }

            await WsWho3Async((ulong)index);
        }


        [Command("wswho4")]
        [Alias("ws4")]
        public async Task WsWho4Async(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var whiteStarList = context.WhiteStarParticipants
                   .Where(p => p.GuildId == Context.Guild.Id)
                   .GroupBy(c => c.WhiteStarIndex);

                foreach (var ws in whiteStarList)
                {
                    var table = new Table("Order", "Name");
                    int index = 0;
                    for (int i = 0; i < ws.Count(); i++)
                    {
                        var name = ws.ElementAt(i).UserName;
                        index++;
                        table.AddRow(index.ToString(), name.FirstLetterToUpperCase().Sanitize());
                    }

                    var alias = GetAliasFromIndex((int)ws.Key);
                    var wsName = string.IsNullOrEmpty(alias) ? ws.Key.ToString() : alias;
                    await ReplyAsync($"***White Star #{wsName}" + table.ToString());
                }
            }
        }
        [Command("wswho4")]
        [Alias("ws4")]
        public async Task WsWho4Async(string alias)
        {
            var index = GetIndexFromAlias(alias);
            if (index == -1)
            {
                await ReplyAsync($"There is no Alias for '{alias}'");
                return;
            }

            await WsWho4Async((ulong)index);
        }

        #endregion

        #region unjoin
        public string UnJoinWhiteStar(ulong userId)
        {
            //Get Role
            using (var context = new BotContext())
            {
                var record = context.WhiteStarParticipants.FirstOrDefault(x => x.UserId == userId &&
                                                                                x.GuildId == Context.Guild.Id);
                if (record != null)
                {
                    var alias = GetAliasFromIndex((int)record.WhiteStarIndex);
                    var wsName = string.IsNullOrEmpty(alias) ? record.WhiteStarIndex.ToString() : alias;
                    var message = $"<@{userId}> has unenrolled for the next White Star #{wsName}!";
                    //delete
                    context.WhiteStarParticipants.Remove(record);
                    context.SaveChanges();

                    return message;
                }
                else
                {
                    return $"<@{userId}> is already ***NOT*** enrolled for the next white star!";
                }

            }
        }

        [Command("unjoin")]
        [Alias("uj", "unjim")]
        public async Task WsUnjoinAsync(SocketGuildUser user)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var message = UnJoinWhiteStar(user.Id);

            await ReplyAsync(message);

            //await Context.Message.DeleteAsync();
        }


        [Command("unjoin")]
        [Alias("uj", "unjim")]
        public async Task WsUnjoinAsync()
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var message = UnJoinWhiteStar(Context.User.Id);

            await ReplyAsync(message);

            //await Context.Message.DeleteAsync();
        }
        #endregion

        #region Description
        [Command("wsdesc")]
        [Alias("wsd")]
        public async Task DescriptionAsync()
        {
            try
            {
                var guildId = Context.Guild.Id;
                if (guildId == 0)
                {
                    await ReplyAsync("Use command from inside a Discord Guild");
                    return;
                }

                using (var context = new BotContext())
                {
                    var members = context.WhiteStarParticipants
                       .Where(p => p.GuildId == guildId)
                       .OrderBy(m => m.AcceptDate)
                       .ToList();

                    var table = new Table("Name", "Description");

                    foreach (var m in members)
                    {
                        var d = m.Description?.ToLower() ?? "";
                        table.AddRow(m.UserName.FirstLetterToUpperCase().Truncate(4), d);
                    }

                    await ReplyAsync(table.ToString());
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [Command("wssetdesc")]
        [Alias("wssd")]
        public async Task SetDescriptionAsync(string desc)
        {
            try
            {
                var guildId = Context.Guild.Id;
                if (guildId == 0)
                {
                    await ReplyAsync("Use command from inside a Discord Guild");
                    return;
                }

                using (var context = new BotContext())
                {

                    var member = context.WhiteStarParticipants
                       .Where(p => p.GuildId == guildId)
                       .FirstOrDefault(m => m.UserId == Context.User.Id &&
                                            m.GuildId == guildId);

                    if (member == null)
                    {
                        await ReplyAsync("You need to !join the white star **before** you set your description.");
                        return;
                    }

                    member.Description = desc;

                    context.SaveChanges();

                    //update
                    await ReplyAsync($@"<@{Context.User.Id}> has set their white star description to: ```{desc}```");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        #endregion


        [Command("wsclear")]
        [Alias("wsc")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsClearAsync(ulong whiteStarIndex = 0)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var participants = context.WhiteStarParticipants
                    .Where(wsp => wsp.GuildId == Context.Guild.Id &&
                                  wsp.WhiteStarIndex == whiteStarIndex);

                context.WhiteStarParticipants.RemoveRange(participants);

                context.SaveChanges();

                //update
                await ReplyAsync($"<@{Context.User.Id}> has cleared White Star Log #{whiteStarIndex}!");
            }
        }

        [Command("wsclear")]
        [Alias("wsc")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsClearAsync(string alias)
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var index = GetIndexFromAlias(alias);
            if (index == -1)
            {
                await ReplyAsync($"There is no Alias for '{alias}'");
                return;
            }

            using (var context = new BotContext())
            {
                var participants = context.WhiteStarParticipants
                     .Where(wsp => wsp.GuildId == Context.Guild.Id &&
                                   wsp.WhiteStarIndex == (ulong)index);

                context.WhiteStarParticipants.RemoveRange(participants);

                context.SaveChanges();

                //update
                var name = string.IsNullOrEmpty(alias) ? index.ToString() : alias;
                await ReplyAsync($"<@{Context.User.Id}> has cleared White Star Log #{name}!");
            }
        }

        [Command("wsclearall")]
        [Alias("wsca")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task WsClearAllAsync()
        {
            //validation
            if (!IsFromGuildChat())
                return;

            using (var context = new BotContext())
            {
                var participants = context.WhiteStarParticipants
                    .Where(wsp => wsp.GuildId == Context.Guild.Id);

                context.WhiteStarParticipants.RemoveRange(participants);

                context.SaveChanges();

                //update
                await ReplyAsync($"<@{Context.User.Id}> has cleared ***ALL*** White Stars!");
            }
        }

        public class WhoModel
        {
            public string UserName { get; set; }
            public string Nickname { get; set; }
            public int CurrentInfluence { get; set; }
            public DateTime AcceptedDate { get; set; }
            public ulong GuildId { get; set; }
            public ulong UserID { get; set; }
        }
    }
}
