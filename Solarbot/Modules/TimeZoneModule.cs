﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class TimeZoneModule : BaseSolarbotModule
    {
        [Command("timezonelist")]
        [Alias("tzl")]
        public async Task TimeZoneList()
        {
            var channel = await Context.User.GetOrCreateDMChannelAsync();

            var table = new Table("Id", "Time Zone");
            var index = 0;
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
            {
                table.AddRow(index.ToString(), z.DisplayName);
                index++;
            }
            var message = table.ToStringNoFormat().ChunksUpto(1971);
            foreach (var m in message)
            {
                await channel.SendMessageAsync("```prolog\n" + m + "```");
                //await ReplyAsync("```prolog\n" + m + "```");
            }

            await Context.Message.DeleteAsync();
        }

        [Command("timezone")]
        [Alias("tz")]
        public async Task TimeZone()
        {

            using (var context = new BotContext())
            {
                var user = context.Users.Include(x => x.TimeZone).FirstOrDefault(u => u.Id == Context.User.Id);
                if (string.IsNullOrEmpty(user.TimeZone?.TimeZoneId))
                {
                    await ReplyAsync($"No local time for {Context.User.DisplayName()}. Use !stz to set your time zone.");
                    return;
                }
                var t = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, user.TimeZone.TimeZoneId).ToString("h\\:mm\\:ss tt");
                await ReplyAsync($"Local time for {Context.User.DisplayName()} is {t}");
            }
            await Context.Message.DeleteAsync();
        }

        [Command("timezone")]
        [Alias("tz")]
        public async Task TimeZone(string name)
        {
            using (var context = new BotContext())
            {
                Users user;
                if (Context.Message.MentionedUsers.Count > 0)
                {

                    var id = Context.Message.MentionedUsers.ElementAt(0).Id;
                    user = context.Users.Include(x => x.TimeZone).FirstOrDefault(u => u.Id == id);

                    if (user == null)
                    {
                        await ReplyAsync($"Never seen <@{id}> before.");
                        return;
                    }
                }
                else
                {
                    user = context.Users.Include(x => x.TimeZone)
                             .FirstOrDefault(m => string.Equals(m.Nickname, name, StringComparison.OrdinalIgnoreCase) ||
                                         string.Equals(m.UserName, name, StringComparison.OrdinalIgnoreCase));

                    if (user == null)
                    {
                        await ReplyAsync($"Never seen {name} before.");
                        return;
                    }
                }

                if (string.IsNullOrEmpty(user.TimeZone?.TimeZoneId))
                {
                    await ReplyAsync($"No local time for {user.DisplayName}. Use !stz to set your time zone.");
                    return;
                }
                var t = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, user.TimeZone.TimeZoneId).ToString("h\\:mm\\:ss tt");
                await ReplyAsync($"Local time for {user.DisplayName} is {t}");

                await Context.Message.DeleteAsync();
            }
        }


        [Command("settimezone")]
        [Alias("stz")]
        public async Task SetTimeZone(int index)
        {
            var message = EditTimeZone(Context.User.Id, index);

            await ReplyAsync(message);

            await Context.Message.DeleteAsync();
        }

        [Command("settimezone")]
        [Alias("stz")]
        public async Task SetTimeZone(SocketGuildUser user, int index)
        {
            var message = EditTimeZone(user.Id, index);

            await ReplyAsync(message);

            await Context.Message.DeleteAsync();
        }

        private string EditTimeZone(ulong id, int index)
        {
            var list = TimeZoneInfo.GetSystemTimeZones();
            var tz = list[index];
            var t = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, tz.Id).ToString("h\\:mm\\:ss tt");

            using (var context = new BotContext())
            {
                string username = "";
                var record = context.TimeZone.Include(x => x.User).FirstOrDefault(u => u.UserId == id);
                if (record == null)
                {
                    //Create
                    context.TimeZone.Add(new Data.TimeZone
                    {
                        UserId = id,
                        TimeZoneId = tz.Id
                    });

                    username = context.Users.FirstOrDefault(u => u.Id == id).DisplayName;
                }
                else
                {
                    //Edit
                    record.TimeZoneId = tz.Id;

                    username = record.User.DisplayName;
                }
                context.SaveChanges();

                return $"```prolog\n {username} has set {index}: {tz.DisplayName}\n LocalTime is {t} ```";
            }
        }

        [Command("channeltimezone")]
        [Alias("ctz")]
        public async Task ChannelTimeZone()
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var cUsers = (Context.Channel as SocketTextChannel)?.Users;

            var table = new Table("Name", "Local Time");

            var list = TimeZoneInfo.GetSystemTimeZones();

            using (var context = new BotContext())
            {
                var users = context.TimeZone.Include(x => x.User).Where(tz => 
                        tz.User.GuildId == Context.Guild.Id &&
                        cUsers.Any(x => x.Id == tz.UserId && x.IsBot == false));
                foreach (var usr in users)
                {
                    var tzid = usr.TimeZoneId;
                    var t = (!string.IsNullOrEmpty(tzid)) ?
                        TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, tzid).ToString("h\\:mm\\:ss tt")
                        : "";

                    table.AddRow(usr.User.DisplayName, t);
                }
            }
            foreach (var chunk in table.AsStringChunks())
            {
                await ReplyAsync(chunk);
            }

            await Context.Message.DeleteAsync();
        }

        [Command("channeltimezoneall")]
        [Alias("ctza")]
        public async Task ChannelTimeZoneAll()
        {
            //validation
            if (!IsFromGuildChat())
                return;

            var cUsers = (Context.Channel as SocketTextChannel)?.Users;

            var table = new Table("Name", "Local Time", "ID", "Desc");

            var list = TimeZoneInfo.GetSystemTimeZones();
            using (var context = new BotContext())
            {
                var users = context.TimeZone.Include(x => x.User).Where(tz =>
                        tz.User.GuildId == Context.Guild.Id &&
                        cUsers.Any(x => x.Id == tz.UserId && x.IsBot == false));
                foreach (var usr in users)
                {
                    var tzid = usr.TimeZoneId;
                    var t = (!string.IsNullOrEmpty(tzid)) ?
                        TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, tzid).ToString("h\\:mm\\:ss tt")
                        : "";
                    var info = TimeZoneInfo.FindSystemTimeZoneById(tzid);

                    table.AddRow(usr.User.DisplayName, t, list.IndexOf(info).ToString() , tzid);
                }
            }
            await ReplyAsync(table.ToString());

            await Context.Message.DeleteAsync();
        }
    }
}
