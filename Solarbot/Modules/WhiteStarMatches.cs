﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Exceptions;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class WhiteStarMatches : ModuleBase<SocketCommandContext>
    {
        public const string AdminRole = "Reporter";
        private static Dictionary<ulong, DeleteCorpModel> MessageCache = new Dictionary<ulong, DeleteCorpModel>();

        [Command("RegisterCorp")]
        [Alias("rc")]
        public async Task RegisterCorp(string corpName)
        {
            //validation
            if (!await IsReporterAsync())
                return;

            await CreateCorpAsync(corpName);
        }

        [Command("RegisterCorp")]
        [Alias("rc")]
        public async Task RegisterCorp(string corpName, ulong win, ulong loss, ulong tie, ulong relicsTot, ulong relicsConcede)
        {
            if (((SocketGuildUser)Context.User).Roles.Any(r => r.Name == AdminRole) == false)
            {
                await ReplyAsync($"{Context.User.DisplayName()} does not have the role of '{AdminRole}' ...");
                return;
            }

            await CreateCorpAsync(corpName, win, loss, tie, relicsTot, relicsConcede);
        }

        public async Task CreateCorpAsync(string corpName, ulong win = 0, ulong loss = 0, ulong tie = 0, ulong relicsTot = 0, ulong relicsConcede = 0)
        {
            try
            {
                using (var context = new BotContext())
                {
                    var corp = new Corp()
                    {
                        Name = corpName.FirstLetterToUpperCase(),
                        Loss = loss,
                        Win = win,
                        Tie = tie,
                        RelicsConceded = relicsConcede,
                        RelicsTotal = relicsTot,
                    };
                    context.Corp.Add(corp);

                    context.CorpLog.Add(new CorpLog()
                    {
                        CorpId = corp.Id,
                        CreatedDate = DateTime.Now,
                        ModifiedUserId = Context.User.Id,
                        ModifiedUserName = Context.User.Username,
                        Type = "Created"
                    });
                    context.SaveChanges();

                    await ReplyAsync($"<@{Context.User.Id}> has **Created** the corp, '{corp.Name}'!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Command("Corps")]
        [Alias("C")]
        public async Task DescriptionAsync()
        {
            try
            {
                using (var context = new BotContext())
                {
                    var corps = context.Corp
                        .Where(c => c.Deleted == false)
                       .OrderBy(m => m.Name)
                       .ToList();

                    var table = new Table("Id", "Corp Name", "W", "L", "T", "Relics", "Conceded");

                    foreach (var c in corps)
                    {
                        table.AddRow(c.Id.ToString(),
                                    c.Name.FirstLetterToUpperCase().Truncate(15),
                                    c.Win.ToString(),
                                    c.Loss.ToString(),
                                    c.Tie.ToString(),
                                    c.RelicsTotal.ToString(),
                                    c.RelicsConceded.ToString());
                    }

                    foreach (string chunk in table.AsStringChunks())
                    {
                        await ReplyAsync(chunk);
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public async Task DeleteCorpAsync(ulong id)
        {
            try
            {
                using (var context = new BotContext())
                {
                    var corp = context.Corp.FirstOrDefault(c => c.Id == id);
                    if (corp == null)
                    {
                        await ReplyAsync($"``` No corp exists with id: {id}```");
                        return;
                    }

                    corp.Deleted = true;

                    context.CorpLog.Add(new CorpLog()
                    {
                        CorpId = corp.Id,
                        CreatedDate = DateTime.Now,
                        ModifiedUserId = Context.User.Id,
                        ModifiedUserName = Context.User.Username,
                        Type = "Deleted"
                    });
                    context.SaveChanges();

                    await ReplyAsync($"<@{Context.User.Id}> has **Deleted** the corp, '{corp.Name}'!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Command("DeleteCorp")]
        [Alias("dc")]
        public async Task DeleteCorpCommandAsync(ulong id)
        {
            //validation
            if (!await IsReporterAsync())
                return;

            DeleteCorpAsync(id);

        }

        #region Validation
        public async Task<bool> IsReporterAsync()
        {
            var hasRole = ((SocketGuildUser)Context.User).Roles.Any(r => r.Name == AdminRole);
            if (hasRole == false)
                await ReplyAsync($"{Context.User.DisplayName()} does not have the role of '{AdminRole}' ...");

            return hasRole;
        }
        #endregion
    }

}

internal class DeleteCorpModel
{
}
