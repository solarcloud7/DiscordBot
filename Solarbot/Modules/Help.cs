﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Models;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Solarbot.Modules
{
    public class Help : ModuleBase<SocketCommandContext>
    {
        const string DiscordLink = "https://discord.gg/HcRhqpa";

        [Command("help")]
        public async Task HelpAsync()
        {
            //await ReplyAsync(GetHelp());
            await ReplyAsync($"Learn more about Cloud Bot and get support at:\n {DiscordLink}");

            await Task.CompletedTask;
        }

        public static string GetHelp()
        {
            //TODO update commands

            var commands = "";

            //WhiteStarLog
            var ws = new Table("Command", "Alias", "Description");
            ws.AddRow("!join", "j", "Add yourself to the white star list");
            ws.AddRow("!join [name]", "j", "Add someone else to the white star list");
            ws.AddRow("!unjoin ", "uj", "Remove yourself from the white star list");
            ws.AddRow("!unjoin [name]", "uj", "Remove someone else from the white star list");
            ws.AddRow("!wswho1", "ws1", "List of participants (Influence)");
            ws.AddRow("!wswho2", "ws2", "List of participants (Time)");
            ws.AddRow("!wsclear", "wsc", "Clear the white star list");
            ws.AddRow("!wsrole [role]", "wsr", "Sets Role that can join a white star");
            commands += ws.ToString();
            commands += "\n";

            //UserStats
            var us = new Table("Command", "Alias", "Description");
            us.AddRow("!guildstats1", "!gs1", "Graph progress of Corporation (style 1)");
            us.AddRow("!guildstats2", "!gs2", "Graph progress of Corporation (style 2)");
            us.AddRow("!guildstats3", "!gs3", "List progress of Corporation");
            us.AddRow("!stat [name]", "!s", "Graph progress of Individual");
            us.AddRow("!logstat [level] [influence]", "!ls", "Create daily log of personal lvl/influence");
            us.AddRow("!delstat", "!ds", "Delete daily log of personal lvl/influence");
            commands += us.ToString();
            commands += "\n";

            //User Info
            var ui = new Table("Command", "Description");
            ui.AddRow("!seen [name]", "Time since online");
            commands += ui.ToString();
            commands += "\n";

            return commands;
        }

        [Command("welcome")]
        public async Task WelcomeAsync()
        {
            UserManager.WelcomeEmbed(Context.User as SocketGuildUser);

            await Task.CompletedTask;
        }

        [Command("support")]
        [Alias("sup")]
        public async Task SupportAsync()
        {
            await ReplyAsync($"Learn more about Cloud Bot and get support at:\n {DiscordLink}");

            await Task.CompletedTask;
        }
    }


}
