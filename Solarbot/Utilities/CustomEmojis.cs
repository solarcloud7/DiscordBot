﻿using Solarbot.Data;
using System.Collections.Generic;

namespace Solarbot.Utilities
{
    public static class CustEmojis
    {
        private static Dictionary<ShipModule, string> Code = new Dictionary<ShipModule, string>
            {
                { ShipModule.Alphadrone, "<:alphadrone:487329341072605204>" },
                { ShipModule.Alpharocket, "<:alpharocket:487329341714333717>" },
                { ShipModule.Alphashield, "<:alphashield:487328007934181376>" },
                { ShipModule.Areashield, "<:areashield:487328007586054146>" },
                { ShipModule.Barrage, "<:barrage:487327934235934731>" },
                { ShipModule.Barrier, "<:barrier:487329341814865941>" },
                { ShipModule.Battery, "<:battery:487327936131760128>" },
                { ShipModule.Battleship1, "<:Battleship1:487329123774103553>" },
                { ShipModule.Battleship2, "<:Battleship2:487329123920904213>" },
                { ShipModule.Battleship3, "<:Battleship3:487329123618783233>" },
                { ShipModule.Battleship4, "<:Battleship4:487329123719577618>" },
                { ShipModule.Battleship5, "<:Battleship5:487329123929161728>" },
                { ShipModule.Blastshield, "<:blastshield:487328008009678858>" },
                { ShipModule.Bond, "<:bond:487329342247141406>" },
                { ShipModule.Cargobayextension, "<:cargobayextension:487327827876904990>" },
                { ShipModule.Crunch, "<:crunch:487327866640400421>" },
                { ShipModule.Deltarocket, "<:deltarocket:487329341919723531>" },
                { ShipModule.Deltashield, "<:deltashield:487328007774666753>" },
                { ShipModule.Destiny, "<:destiny:487329342058266624>" },
                { ShipModule.Duallaser, "<:duallaser:487327935884427277>" },
                { ShipModule.Emp, "<:emp:487329342339153920>" },
                { ShipModule.Enrich, "<:enrich:487327866900447253>" },
                { ShipModule.Entrust, "<:entrust:487327825997594624>" },
                { ShipModule.Fortify, "<:fortify:487329342121312258>" },
                { ShipModule.Genesis, "<:genesis:487327866703314945>" },
                { ShipModule.Hydrogenbayextension, "<:hydrogenbayextension:487327867005304837>" },
                { ShipModule.Hydrogenupload, "<:hydrogenupload:487327866841726978>" },
                { ShipModule.Impulse, "<:impulse:487329339340488704>" },
                { ShipModule.Laser, "<:laser:487327936274235412>" },
                { ShipModule.Leap, "<:leap:487329339361198090>" },
                { ShipModule.Massbattery, "<:massbattery:487327936207257600>" },
                { ShipModule.Miner1, "<:Miner1:487329123761520642>" },
                { ShipModule.Miner2, "<:Miner2:487329124051058698>" },
                { ShipModule.Miner3, "<:Miner3:487329123967172629>" },
                { ShipModule.Miner4, "<:Miner4:487329123983818753>" },
                { ShipModule.Miner5, "<:Miner5:487329123560063017>" },
                { ShipModule.Miningboost, "<:miningboost:487327867026407424>" },
                { ShipModule.Miningunity, "<:miningunity:487327867060092928>" },
                { ShipModule.Mirrorshield, "<:mirrorshield:487328007648968735>" },
                { ShipModule.Offload, "<:offload:487327825809113099>" },
                { ShipModule.Omegarocket, "<:omegarocket:487329341865459713>" },
                { ShipModule.Omegashield, "<:omegashield:487328007707557889>" },
                { ShipModule.Passiveshield, "<:passiveshield:487328007665745932>" },
                { ShipModule.Recall, "<:recall:487327826026954802>" },
                { ShipModule.Redstarlifeextender, "<:redstarlifeextender:487329342360125440>" },
                { ShipModule.Remotemining, "<:remotemining:487327867059830784>" },
                { ShipModule.Remoterepair, "<:remoterepair:487329342335090692>" },
                { ShipModule.Rush, "<:rush:487327826043994142>" },
                { ShipModule.Salvage, "<:salvage:487329342259724288>" },
                { ShipModule.Sanctuary, "<:sanctuary:487329339268923412>" },
                { ShipModule.Shipmentbeam, "<:shipmentbeam:487327827553812481>" },
                { ShipModule.Shipmentcomputer, "<:Shipmentcomputer:487327826052251658>" },
                { ShipModule.Shipmentdrone, "<:shipmentdrone:487327827461668896>" },
                { ShipModule.Stealth, "<:stealth:487329341802414081>" },
                { ShipModule.Suppress, "<:suppress:487329342419107843>" },
                { ShipModule.Teleport, "<:teleport:487329342364581891>" },
                { ShipModule.Timewarp, "<:timewarp:487329342310055946>" },
                { ShipModule.Tradeboost, "<:tradeboost:487327828153466900>" },
                { ShipModule.Tradeburst, "<:tradeburst:487327827721584641>" },
                { ShipModule.Transport1, "<:Transport1:487329124088545290>" },
                { ShipModule.Transport2, "<:Transport2:487329124365631508>" },
                { ShipModule.Transport3, "<:Transport3:487329124055121920>" },
                { ShipModule.Transport4, "<:Transport4:487329124088807424>" },
                { ShipModule.Transport5, "<:Transport5:487329124092739584>" },
                { ShipModule.Unity, "<:unity:487329342355931151>" },
                { ShipModule.Vengeance, "<:vengeance:487329342406393866>" },
                { ShipModule.Weakbattery, "<:weakbattery:487327935834095628>" },
            };

        public static string ToCode(this ShipModule ce)
        {
            return Code[ce];
        }

        public static int GetStat(this UserModules user, ShipModule mod)
        {
            switch (mod)
            {
                case ShipModule.Alphadrone:
                    return user.Alphadrone;
                case ShipModule.Alpharocket:
                    return user.Alpharocket;
                case ShipModule.Alphashield:
                    return user.Alphashield;
                case ShipModule.Areashield:
                    return user.Areashield;
                case ShipModule.Barrage:
                    return user.Barrage;
                case ShipModule.Barrier:
                    return user.Barrier;
                case ShipModule.Battery:
                    return user.Battery;
                case ShipModule.Battleship1:
                case ShipModule.Battleship2:
                case ShipModule.Battleship3:
                case ShipModule.Battleship4:
                case ShipModule.Battleship5:
                    return user.Battleship;
                case ShipModule.Blastshield:
                    return user.Blastshield;
                case ShipModule.Bond:
                    return user.Bond;
                case ShipModule.Cargobayextension:
                    return user.Cargobayextension;
                case ShipModule.Crunch:
                    return user.Crunch;
                case ShipModule.Deltarocket:
                    return user.Deltarocket;
                case ShipModule.Deltashield:
                    return user.Deltashield;
                case ShipModule.Destiny:
                    return user.Destiny;
                case ShipModule.Duallaser:
                    return user.Duallaser;
                case ShipModule.Emp:
                    return user.Emp;
                case ShipModule.Enrich:
                    return user.Enrich;
                case ShipModule.Entrust:
                    return user.Entrust;
                case ShipModule.Fortify:
                    return user.Fortify;
                case ShipModule.Genesis:
                    return user.Genesis;
                case ShipModule.Hydrogenbayextension:
                    return user.Hydrogenbayextension;
                case ShipModule.Hydrogenupload:
                    return user.Hydrogenupload;
                case ShipModule.Impulse:
                    return user.Impulse;
                case ShipModule.Laser:
                    return user.Laser;
                case ShipModule.Leap:
                    return user.Leap;
                case ShipModule.Massbattery:
                    return user.Massbattery;
                case ShipModule.Miner1:
                case ShipModule.Miner2:
                case ShipModule.Miner3:
                case ShipModule.Miner4:
                case ShipModule.Miner5:
                    return user.Miner;
                case ShipModule.Miningboost:
                    return user.Miningboost;
                case ShipModule.Miningunity:
                    return user.Miningunity;
                case ShipModule.Mirrorshield:
                    return user.Mirrorshield;
                case ShipModule.Offload:
                    return user.Offload;
                case ShipModule.Omegarocket:
                    return user.Omegarocket;
                case ShipModule.Omegashield:
                    return user.Omegashield;
                case ShipModule.Passiveshield:
                    return user.Passiveshield;
                case ShipModule.Recall:
                    return user.Recall;
                case ShipModule.Redstarlifeextender:
                    return user.Redstarlifeextender;
                case ShipModule.Remotemining:
                    return user.Remotemining;
                case ShipModule.Remoterepair:
                    return user.Remoterepair;
                case ShipModule.Rush:
                    return user.Rush;
                case ShipModule.Salvage:
                    return user.Salvage;
                case ShipModule.Sanctuary:
                    return user.Sanctuary;
                case ShipModule.Shipmentbeam:
                    return user.Shipmentbeam;
                case ShipModule.Shipmentcomputer:
                    return user.Shipmentcomputer;
                case ShipModule.Shipmentdrone:
                    return user.Shipmentdrone;
                case ShipModule.Stealth:
                    return user.Stealth;
                case ShipModule.Suppress:
                    return user.Suppress;
                case ShipModule.Teleport:
                    return user.Teleport;
                case ShipModule.Timewarp:
                    return user.Timewarp;
                case ShipModule.Tradeboost:
                    return user.Tradeboost;
                case ShipModule.Tradeburst:
                    return user.Tradeburst;
                case ShipModule.Transport1:
                case ShipModule.Transport2:
                case ShipModule.Transport3:
                case ShipModule.Transport4:
                case ShipModule.Transport5:
                    return user.Transport;
                case ShipModule.Unity:
                    return user.Unity;
                case ShipModule.Vengeance:
                    return user.Vengeance;
                case ShipModule.Weakbattery:
                    return user.Weakbattery;

            }
            //if here, error
            return -1;
        }

    }



    public enum ShipModule
    {
        Sanctuary = 1,
        Hydrogenbayextension = 2,
        Miningboost = 3,
        Remotemining = 4,
        Miningunity = 5,
        Barrage = 6,
        Weakbattery = 7,
        Duallaser = 8,
        Battery = 9,
        Massbattery = 10,
        Laser = 11,
        Areashield = 12,
        Mirrorshield = 13,
        Passiveshield = 14,
        Omegashield = 15,
        Enrich = 16,
        Deltashield = 17,
        Blastshield = 18,
        Miner5 = 19,
        Battleship3 = 20,
        Battleship4 = 21,
        Miner1 = 22,
        Battleship1 = 23,
        Battleship2 = 24,
        Battleship5 = 25,
        Miner3 = 26,
        Miner4 = 27,
        Miner2 = 28,
        Transport3 = 29,
        Transport1 = 30,
        Transport4 = 31,
        Alphashield = 32,
        Transport5 = 33,
        Hydrogenupload = 34,
        Crunch = 35,
        Impulse = 36,
        Leap = 37,
        Alphadrone = 38,
        Alpharocket = 39,
        Stealth = 40,
        Barrier = 41,
        Omegarocket = 42,
        Deltarocket = 43,
        Destiny = 44,
        Fortify = 45,
        Bond = 46,
        Salvage = 47,
        Timewarp = 48,
        Remoterepair = 49,
        Genesis = 50,
        Emp = 51,
        Redstarlifeextender = 52,
        Teleport = 53,
        Vengeance = 54,
        Suppress = 55,
        Offload = 56,
        Entrust = 57,
        Recall = 58,
        Rush = 59,
        Shipmentcomputer = 60,
        Shipmentdrone = 61,
        Shipmentbeam = 62,
        Tradeburst = 63,
        Cargobayextension = 64,
        Tradeboost = 65,
        Unity = 66,
        Transport2 = 67,

    }
}
