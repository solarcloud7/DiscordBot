﻿using Discord.Commands;
using Discord.WebSocket;
using Solarbot.Data;
using Solarbot.Extentions;
using Solarbot.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solarbot.Exceptions;
using Solarbot.Models;

namespace Solarbot.Utilities
{
    public class ShipTable
    {
        private List<BaseShips> ships = new List<BaseShips>();
        private bool displayOwner = false;
        private bool displayType = false;
        private bool displayLevel = false;
        private bool displayInfo = false;
        private bool displayWeapon = false;
        private bool displayShield = false;
        private bool displayModules = false;

        public ShipTable()
        {
        }

        /**
         * Retrieve this list of ships as a rendered table
         */
        public string GetRendered()
        {
            return this.GetTable().AsString();
        }

        /**
         * Retrieve the populated underlying Table for this list of Ships
         */
        public Table GetTable() {
            int numOfMods = 0;
            int numOfModCols = 0;
            var columns = new List<string>();
            EquipmentModules shipWeapon;
            EquipmentModules shipShield;
            List<string> shipCells;
            List<EquipmentModules> shipMods;

            if (this.displayOwner)
            {
                columns.Add("Owner");
            }

            if (this.displayType)
            {
                columns.Add("Ship Type");
            }

            if (this.displayLevel)
            {
                columns.Add("Lv");
            }

            if (this.displayInfo)
            {
                columns.Add("Info");
            }

            if (this.displayWeapon)
            {
                columns.Add("Weapon");
            }

            if (this.displayShield)
            {
                columns.Add("Shield");
            }

            if (this.displayModules)
            {
                // Retrieve the most amount of modules any ship has; that will be used to calculate how many columns to display.
                // This is a 2-stage process because different ship levels will have different number of columns, and we need to know
                // the highest number of total columns across all ship types.
                foreach (BaseShips ship in this.ships)
                {
                    numOfMods = 0;
                    foreach (EquipmentModules mod in ship.GetEquippedModules())
                    {
                        if (!
                            (mod.Type == EquipmentModules.TypeShield || mod.Type == EquipmentModules.TypeWeapon)
                           )
                        {
                            // The mod is *not* a weapon or shield; modules other get added to the other "module" sections.
                            numOfMods++;
                            if (numOfMods > numOfModCols)
                            {
                                numOfModCols = numOfMods;
                                columns.Add("Module " + numOfMods.ToString());
                            }
                        }
                    }
                }
            }

            var table = new Table(columns.ToArray());


            // Now, iterate through each ship type and add their details to the table.
            foreach (BaseShips ship in this.ships)
            {
                shipCells = new List<string>();
                var shipMax = ship.GetSupportModuleSlots();
                shipWeapon = null;
                shipShield = null;
                shipMods = new List<EquipmentModules>();

                foreach (EquipmentModules mod in ship.GetEquippedModules())
                {
                    if (mod.Type == EquipmentModules.TypeWeapon)
                    {
                        shipWeapon = mod;
                    }
                    else if (mod.Type == EquipmentModules.TypeShield)
                    {
                        shipShield = mod;
                    }
                    else
                    {
                        shipMods.Add(mod);
                    }
                }

                if (this.displayOwner)
                {
                    shipCells.Add(ship.GetOwner());
                }

                if (this.displayType)
                {
                    shipCells.Add(ship.GetName());
                }

                if (this.displayLevel)
                {
                    shipCells.Add(ship.Level.ToString());
                }

                if (this.displayInfo)
                {
                    shipCells.Add(ship.GetInfo());
                }

                if (this.displayWeapon)
                {
                    shipCells.Add(shipWeapon != null ? shipWeapon.GetShortName() : "");
                }

                if (this.displayShield)
                {
                    shipCells.Add(shipShield != null ? shipShield.GetShortName() : "");
                }

                if (this.displayModules)
                {
                    foreach (EquipmentModules mod in shipMods)
                    {
                        shipCells.Add(mod.GetShortName());
                    }

                    // Empty columns
                    for (var i = shipMods.Count; i < numOfModCols; i++)
                    {
                        shipCells.Add(" ");
                    }
                }

                table.AddRow(shipCells.ToArray());
            }

            return table;
        }

        /**
         * Show the owner name for each ship
         */
        public void ShowOwner()
        {
            this.displayOwner = true;
        }

        /**
         * Show the type of each ship
         */
        public void ShowType()
        {
            this.displayType = true;
        }

        /**
         * Show the levels of each ship
         */
        public void ShowLevel()
        {
            this.displayLevel = true;
        }

        /**
         * Show additional details of each ship
         */
        public void ShowInfo()
        {
            this.displayInfo = true;
        }

        /**
         * Show weapons for each ship
         */
        public void ShowWeapon()
        {
            this.displayWeapon = true;
        }

        /**
         * Show shields for each ship
         */
        public void ShowShield()
        {
            this.displayShield = true;
        }

        /**
         * Show additional modules for each ship
         */
        public void ShowModules()
        {
            this.displayModules = true;
        }

        /**
         * Add a single ship to the stack of ships to display
         */
        public void AddShip(BaseShips ship)
        {
            this.ships.Add(ship);
        }

        /**
         * Add multiple ships to the stack of ships to display
         */
        public void AddShips(List<BaseShips> ships)
        {
            foreach (BaseShips ship in ships)
            {
                this.AddShip(ship);
            }
        }
    }
}
