﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solarbot.Data
{
    public class UserModules
    {
        public int Alphadrone { get; set; }
        public int Alpharocket { get; set; }
        public int Alphashield { get; set; }
        public int Areashield { get; set; }
        public int Barrage { get; set; }
        public int Barrier { get; set; }
        public int Battery { get; set; }
        public int Battleship { get; set; }
        public int Blastshield { get; set; }
        public int Bond { get; set; }
        public int Cargobayextension { get; set; }
        public int Crunch { get; set; }
        public int Deltarocket { get; set; }
        public int Deltashield { get; set; }
        public int Destiny { get; set; }
        public int Duallaser { get; set; }
        public int Emp { get; set; }
        public int Enrich { get; set; }
        public int Entrust { get; set; }
        public int Fortify { get; set; }
        public int Genesis { get; set; }
        public int Hydrogenbayextension { get; set; }
        public int Hydrogenupload { get; set; }
        public int Impulse { get; set; }
        public int Laser { get; set; }
        public int Leap { get; set; }
        public int Massbattery { get; set; }
        public int Miner { get; set; }
        public int Miningboost { get; set; }
        public int Miningunity { get; set; }
        public int Mirrorshield { get; set; }
        public int Offload { get; set; }
        public int Omegarocket { get; set; }
        public int Omegashield { get; set; }
        public int Passiveshield { get; set; }
        public int Recall { get; set; }
        public int Redstarlifeextender { get; set; }
        public int Remotemining { get; set; }
        public int Remoterepair { get; set; }
        public int Rush { get; set; }
        public int Salvage { get; set; }
        public int Sanctuary { get; set; }
        public int Shipmentbeam { get; set; }
        public int Shipmentcomputer { get; set; }
        public int Shipmentdrone { get; set; }
        public int Stealth { get; set; }
        public int Suppress { get; set; }
        public int Teleport { get; set; }
        public int Timewarp { get; set; }
        public int Tradeboost { get; set; }
        public int Tradeburst { get; set; }
        public int Transport { get; set; }
        public int Unity { get; set; }
        public int Vengeance { get; set; }
        public int Weakbattery { get; set; }

    }
}
