﻿using Microsoft.EntityFrameworkCore;
using Solarbot.Extentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Solarbot.Data
{
    public class UserDefaultGuilds
    {
        [Key]
        public ulong UserId { get; set; }
        public ulong GuildId { get; set; }
    }
}
