﻿using Microsoft.EntityFrameworkCore;
using Solarbot.Extentions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Solarbot.Data
{
    public class WhiteStarParticipants
    {
        [Key]
        public ulong UserId { get; set; }
        public ulong GuildId { get; set; }
        public DateTime AcceptDate { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public ulong WhiteStarIndex { get; set; }

        public Users User { get; set; }

    }
}
