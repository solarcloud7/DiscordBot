﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Solarbot.Data
{
    public class GuildSettings
    {
        [Key]
        public ulong Id { get; set; }
        public ulong GuildId { get; set; }
        public string Name { get; set; }
        public int SettingId { get; set; }
        public string Value { get; set; }
        public ulong CreatedId { get; set; }
        public DateTime CreatedDate { get; set; }

        public IBaseSetting GetBaseSetting()
        {
            var t = GetModelType();
            return JsonConvert.DeserializeObject(Value, t) as IBaseSetting;
        }

        public Type GetModelType()
        {
            var type = (Setting)this.SettingId;
            switch (type)
            {
                case Setting.WsWhitelistRole:
                    return typeof(WsWhitelistRole);
                case Setting.WsBlacklistRole:
                    return typeof(WsBlacklistRole);
                case Setting.WhiteStarIndexAlias:
                    return typeof(WhiteStarAlias);
            }

            return typeof(object);
        }
    }

    public enum Setting
    {
        WsWhitelistRole = 0,
        WsBlacklistRole = 1,
        WhiteStarIndexAlias = 2,
        SmsRequiredRole = 3
    }

    public interface IBaseSetting
    {
        string Serialize();
        List<string> Properties();
        List<string> Values();
    }

    public class BaseSetting<T> : IBaseSetting where T : new()
    {
        private string _value;

        public string Serialize()
        {
            _value = JsonConvert.SerializeObject(this);
            return _value;
        }
        public T Deserialize(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static T DeserializeObject(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public List<string> Properties()
        {
            List<string> list = new List<string>();
            foreach (var prop in GetType().GetProperties())
            {
                list.Add(prop.Name);
            }
            return list;
        }

        public static List<string> ModelProperties()
        {
            List<string> list = new List<string>();
            foreach (var prop in new T().GetType().GetProperties())
            {
                list.Add(prop.Name);
            }
            return list;
        }


        public List<string> Values()
        {
            List<string> list = new List<string>();
            foreach (var prop in GetType().GetProperties())
            {
                list.Add(prop.GetValue(this, null).ToString());
            }
            return list;
        }


    }

    public class WhiteStarAlias : BaseSetting<WhiteStarAlias>
    {
        public string Alias { get; set; }
        public int Index { get; set; }
    }
    public class WsWhitelistRole : BaseSetting<WsWhitelistRole>
    {
        public string WhitelistRole { get; set; }
    }
    public class WsBlacklistRole : BaseSetting<WsBlacklistRole>
    {
        public string BlacklistRole { get; set; }
    }

}
