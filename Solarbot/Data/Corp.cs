﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Solarbot.Data
{
    public class Corp
    {
        public ICollection<CorpLog> CorpLogs { get; set; } = new HashSet<CorpLog>();

        [Key]
        public ulong Id { get; set; }
        public string Name { get; set; }
        public ulong Win { get; set; }
        public ulong Loss { get; set; }
        public ulong Tie { get; set; }
        public ulong RelicsTotal { get; set; }
        public ulong RelicsConceded { get; set; }
        public bool Deleted { get; set; }
    }
}
