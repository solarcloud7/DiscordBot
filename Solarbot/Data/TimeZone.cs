﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Solarbot.Data
{
    public class TimeZone
    {
        public Users User { get; set; }

        [Key]
        public ulong UserId { get; set; }
        public string TimeZoneId { get; set; }
    }
}
