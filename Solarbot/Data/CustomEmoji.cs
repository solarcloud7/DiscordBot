﻿using System.ComponentModel.DataAnnotations;

namespace Solarbot.Data
{
    public class CustomEmoji
    {
        [Key]
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsAnimated { get; set; }
        public string GuildName { get; set; }
    }
}
