﻿-- Script Date: 9/6/2018 5:08 PM  - ErikEJ.SqlCeScripting version 3.5.2.75
SELECT [Id]
      ,[Name]
      ,[Code]
      ,[IsAnimated]
  FROM [CustomEmoji]
  Where [IsAnimated] = 0


--Create Enum
Select [Name] || ' = ' || [Id] ||','
From CustomEmoji
Where IsAnimated = 0

--Create Dictionary
Select '{ ShipModule.' || [Name] || ', "' || [Code]  || '" },'
From CustomEmoji
Where IsAnimated = 0
Order By Name

--Create HS Module Table
Select ', [' || [Name] ||'] INTEGER DEFAULT 0 NOT NULL'
From CustomEmoji
Where IsAnimated = 0
Order By Name


--Create C# Model
Select 'public int '|| [Name] ||' { get; set; }'
From CustomEmoji
Where IsAnimated = 0
order by Name

--Create C# Switch
Select 'case ShipModule.' || [Name] || ':
return user.' || [Name] || ';'
From CustomEmoji
Where IsAnimated = 0
order by Name

Delete From CustomEmoji
