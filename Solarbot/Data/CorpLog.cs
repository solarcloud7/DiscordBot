﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Solarbot.Data
{
    public class CorpLog
    {
        public Corp Corp { get; set; }

        [Key]
        public ulong Id { get; set; }
        public string Type { get; set; }
        public ulong ModifiedUserId { get; set; }
        public string ModifiedUserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public ulong CorpId { get; set; }
    }
}
