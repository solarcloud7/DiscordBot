﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Solarbot.Data
{
    public class Settings
    {
        [Key]
        public int Id { get; set; }
        public int SettingId { get; set; }
        public string Description { get; set; }

    }
}
