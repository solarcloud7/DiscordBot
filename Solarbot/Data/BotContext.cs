﻿using Microsoft.EntityFrameworkCore;

namespace Solarbot.Data
{
    public class BotContext : DbContext
    {
        public DbSet<Corp> Corp { get; set; }
        public DbSet<CustomEmoji> CustomEmoji { get; set; }
        public DbSet<CorpLog> CorpLog { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<MemberStats> MemberStats { get; set; }
        public DbSet<WhiteStarParticipants> WhiteStarParticipants { get; set; }
        public DbSet<GuildSettings> GuildSettings { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<TimeZone> TimeZone { get; set; }
        public DbSet<UserDefaultGuilds> UserDefaultGuilds { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Solarbot.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(c => new { c.Id, c.GuildId });

                entity.HasMany(e => e.MemberStats)
                    .WithOne(e => e.User)
                    .HasPrincipalKey(e => e.Id);

            });

            modelBuilder.Entity<WhiteStarParticipants>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.GuildId });

                entity.HasOne(e => e.User)
                    .WithOne(e => e.WhiteStarParticipant)
                    .HasPrincipalKey<Users>(e => e.Id);
            });

            modelBuilder.Entity<MemberStats>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasOne(e => e.User)
                    .WithMany(e => e.MemberStats)
                    .HasForeignKey(e => e.UserId);
            });

            modelBuilder.Entity<GuildSettings>(entity =>
            {
                entity.HasKey(e => e.Id);

                //entity.HasOne(e => e.Setting)
                //    .WithMany(e => e.GuildSettings)
                //    .HasForeignKey(e => e.SettingId);
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.HasKey(e => e.Id);

                //entity.HasMany(e => e.GuildSettings)
                //     .WithOne(e => e.Settings)
                //     .HasForeignKey(e => e.SettingId);
            });

            modelBuilder.Entity<TimeZone>(entity =>
            {
                entity.HasOne(e => e.User)
                    .WithOne(e => e.TimeZone)
                    .HasPrincipalKey<Users>(e => e.Id);
            });

            modelBuilder.Entity<Corp>(entity =>
            {
                entity.HasKey(e => new { e.Id });

                entity.HasMany(e => e.CorpLogs)
                   .WithOne(e => e.Corp)
                   .HasForeignKey(e => e.CorpId);
            });

            modelBuilder.Entity<CorpLog>(entity =>
            {
                entity.HasKey(e => new { e.Id });

                entity.HasOne(e => e.Corp)
                   .WithMany(e => e.CorpLogs)
                   .HasForeignKey(e => e.CorpId);
            });
        }
    }
}
