﻿using System;
using Microsoft.EntityFrameworkCore;
using Solarbot.Extentions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Solarbot.Models.Modules.Trade;
using Solarbot.Models;

namespace Solarbot.Data
{
    public class Users
    {
        public ulong GuildId { get; set; }
        public ulong Id { get; set; }
        public string Nickname { get; set; }
        public string UserName { get; set; }
        public string TimeZoneId { get; set; }
        public uint ModuleTradeCargo { get; set; }
        public uint ModuleMiningBoost { get; set; }
        public uint ModuleMiningEnrich { get; set; }
        public uint ModuleMiningRemote { get; set; }
        public uint ModuleMiningCrunch { get; set; }
        public uint ModuleMiningGenesis { get; set; }
        public uint ModuleShieldAlpha { get; set; }
        public uint ModuleShieldDelta { get; set; }
        public uint ModuleShieldPassive { get; set; }
        public uint ModuleShieldOmega { get; set; }
        public uint ModuleShieldMirror { get; set; }
        public uint ModuleShieldArea { get; set; }
        public uint ModuleSupportEmp { get; set; }
        public uint ModuleSupportTeleport { get; set; }
        public uint ModuleSupportRepair { get; set; }
        public uint ModuleSupportTime { get; set; }
        public uint ModuleSupportUnity { get; set; }
        public uint ModuleSupportStealth { get; set; }
        public uint ModuleSupportFortify { get; set; }
        public uint ModuleSupportImpulse { get; set; }
        public uint ModuleSupportRocket { get; set; }
        public uint ModuleSupportSalvage { get; set; }
        public uint ModuleSupportSuppress { get; set; }
        public uint ModuleSupportDestiny { get; set; }
        public uint ModuleSupportBarrier { get; set; }
        public uint ModuleSupportVengeance { get; set; }
        public uint ModuleSupportLeap { get; set; }
        public uint ModuleSupportAlphaDrone { get; set; }
        public uint ModuleWeaponBattery { get; set; }
        public uint ModuleWeaponLaser { get; set; }
        public uint ModuleWeaponMassBattery { get; set; }
        public uint ModuleWeaponDualLaser { get; set; }
        public uint ModuleWeaponBarrage { get; set; }
        public uint ShipBattleship { get; set; }
        public uint ShipTransport { get; set; }
        public uint ShipMiner { get; set; }
        public string EquippedTransportSupport1 { get; set; } = "";
        public string EquippedBattleshipWeapon { get; set; } = "";
        public string EquippedBattleshipShield { get; set; } = "";
        public string EquippedBattleshipSupport1 { get; set; } = "";
        public string EquippedBattleshipSupport2 { get; set; } = "";
        public string EquippedBattleshipSupport3 { get; set; } = "";
        public string EquippedBattleshipSupport4 { get; set; } = "";
        public DateTime LastSeen { get; set; }


        public ICollection<MemberStats> MemberStats { get; set; } = new HashSet<MemberStats>();
        public TimeZone TimeZone { get; set; }
        public WhiteStarParticipants WhiteStarParticipant { get; set; }
        
        /**
         * Get this user's DisplayName (or Nickname if that is set instead), up to N characters
         */
        public string DisplayName
        {
            get
            {
                string name = (!string.IsNullOrEmpty(Nickname)) ? this.Nickname : this.UserName;

                if (name.Length > 18 && name.IndexOf(" ") != -1)
                {
                    // Attempt to reduce the length of the name, should be OK since there's a space.
                    name = name.Substring(0, name.IndexOf(" "));
                }

                if (name.Length > 18)
                {
                    // Name is *still* too long, just force it!
                    name = name.Substring(0, 18);
                }
                return name.FirstLetterToUpperCase()
                    .Replace(".", "") // Only remove "." from names.  It breaks  ```prolog formatting
                    .Sanitize();
            }
        }

        /**
         * Get a List of all player modules they have set.
         * 
         * Any module with a level of 0 (not set), will *not* be returned.
         */
        public List<EquipmentModules> GetEquipmentModules()
        {
            var mods = new List<EquipmentModules>();
            EquipmentModules mod;

            foreach (var name in EquipmentModules.ModuleNames)
            {
                mod = EquipmentModules.EquipmentFromName(name);
                mod.Fromuser(this);

                if (mod.Level > 0)
                {
                    mods.Add(mod);
                }
            }

            return mods;
        }

        /**
         * Get a List of all player ship levels they have set.
         * 
         * This will generally only return 3 entries, as there are only 3 types of ships.
         */
        public List<BaseShips> GetShips()
        {
            var ships = new List<BaseShips>();
            //string[] shipnames = { "battleship", "transport", "miner" };
            string[] shipnames = { "battleship", "transport" };
            BaseShips s;

            foreach (var name in shipnames)
            {
                s = BaseShips.ShipFromName(name);
                s.Fromuser(this);
                ships.Add(s);
            }

            return ships;
        }

        public EquipmentModules GetEquipmentModule(string modname)
        {
            EquipmentModules mod = EquipmentModules.EquipmentFromName(modname);
            mod.Fromuser(this);

            return mod;
        }

        public BaseShips GetShip(string shipname)
        {
            BaseShips ship = BaseShips.ShipFromName(shipname);
            ship.Fromuser(this);

            return ship;
        }
    }
}
