﻿using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Solarbot.Data;
using System.IO;
using Solarbot.Models;
using Solarbot.Exceptions;

namespace Solarbot
{
    public class Program
    {
        private static IConfiguration Configuration { get; set; }

        public static List<CommandInfo> Commands { get; set; }

        static void Main(string[] args)
        {
            try
            {
                var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json");

                Configuration = builder.Build();

                Configuration.GetSection("App").Get<AppConfig>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                return;
            }

            new Program().RunBotAsync().GetAwaiter().GetResult();
        }

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;
        public async Task RunBotAsync()
        {

            _client = new DiscordSocketClient();
            _commands = new CommandService();
            _services = new ServiceCollection()
                        .AddSingleton(_client)
                        .AddSingleton(_commands)
                        .BuildServiceProvider();

            string botToken = (IsDebug) ? AppConfig.DebugBotToken : AppConfig.BotToken;

            Debug($"{(IsDebug ? "DEBUG" : "PROD")}: Connecting to Discord using the token {botToken.Substring(0, 6)}***************");

            //event subscriptions
            _client.Log += Log;
            _client.Ready += Ready;
            _client.GuildMemberUpdated += GuildMemberUpdatedAsync;

            await RegisterCommandsAsync();

            await _client.LoginAsync(Discord.TokenType.Bot, botToken);

            await _client.StartAsync();

            await _client.SetGameAsync("Hades Star");

            await UserListenerAsync();

            await Task.Delay(-1);
        }


        private Task Ready()
        {
            UserManager.InitUsers(_client.Guilds?.ToList());

            foreach (var g in _client.Guilds)
            {
                using (var context = new BotContext())
                {
                    //if (!context.GuildSettings.Any(guild => guild.GuildId == g.Id))
                    //{
                    //    context.GuildSettings.Add(new GuildSettings
                    //    {
                    //        GuildId = g.Id,
                    //        Name = g.Name
                    //    });
                    //    context.SaveChanges();
                    //}
                }
            }

            return Task.CompletedTask;
        }

        static public void Debug(String message)
        {
            if (IsDebug)
                Console.WriteLine(message);
        }

        public static bool IsDebug
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }


        private async Task GuildMemberUpdatedAsync(SocketGuildUser arg1, SocketGuildUser arg2)
        {
            Console.WriteLine((arg1.Nickname ?? arg1.Username) + " : " + arg2.Status);

            switch (arg2.Status)
            {
                case UserStatus.Offline:
                    UserManager.Away(arg2);
                    break;
                case UserStatus.Online:
                    UserManager.Login(arg2);
                    break;
                case UserStatus.Idle:
                    UserManager.Away(arg2);
                    break;
                case UserStatus.AFK:
                    UserManager.Away(arg2);
                    break;
                case UserStatus.DoNotDisturb:
                    UserManager.Away(arg2);
                    break;
                case UserStatus.Invisible:
                    UserManager.Away(arg2);
                    break;
            }

        }

        #region Dynamic Text Response
        private async Task UserListenerAsync()
        {
            var m = await GetInputAsync();
            if (!string.IsNullOrEmpty(m))
            {

                foreach (var guild in _client.Guilds)
                {
                    var channel = guild.DefaultChannel ?? guild.TextChannels.FirstOrDefault();
                    if (channel != null)
                    {
                        await channel.SendMessageAsync(m);
                    }
                }
            }

            await UserListenerAsync();
        }

        public async Task<string> GetInputAsync()
        {
            return await Task.Run(() => Console.ReadLine());
        }
        #endregion



        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg);

            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());

            Commands = _commands.Commands.ToList();
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            SocketCommandContext context;
            int argPos = 0;
            IResult result;
            string helpMessage;
            CommandInfo cmd;
            bool isPrivateMessage = (arg.Channel.GetType() == typeof(SocketDMChannel));

            // If there was no message or the source was a bot, simply do nothing.
            if (message == null || message.Author.IsBot) return;

            //print private messages
            if (isPrivateMessage)
            {
                var m = $"DM {DateTime.Now.ToString("M/d h:mm tt")} - {arg.Author.Username}: {message.Content}";
                Console.WriteLine(m);
            }


            if (message.HasStringPrefix("!", ref argPos) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                // Retrieves the context of the message in a format that Discord will understand natively.
                context = new SocketCommandContext(_client, message);

                try
                {
                    result = await _commands.ExecuteAsync(context, argPos, _services);

                     if (result.Error != null)
                    {
                        throw new UserException(result.ErrorReason, result.ErrorReason , UserException.EMOJI_PUZZLED);
                    }
                }
                catch (UserException ex)
                {
                    if (ex.Emoji != null)
                    {
                        //await message.AddReactionAsync(new Emoji(ex.Emoji));
                    }

                    if (isPrivateMessage)
                    {
                        await message.Author.SendMessageAsync(ex.UserFriendlyMessage);
                    }
                    else
                    {
                        await message.Channel.SendMessageAsync(ex.UserFriendlyMessage);
                    }
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
            }
        }
    }
}
