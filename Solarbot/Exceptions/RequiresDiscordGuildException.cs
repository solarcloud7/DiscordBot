﻿using System;
namespace Solarbot.Exceptions
{
    public class RequiresDiscordGuildException : UserException
    {
        public RequiresDiscordGuildException() : base("Command executed outside of Discord Guild", "")
        {
            this.UserFriendlyMessage = "Please run that command from inside a Discord Guild.";
        }
    }
}
