﻿using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Solarbot.Data;
using Solarbot.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Solarbot
{
    public static class UserManager
    {
        public static List<SocketGuildUser> OnlineUsers = new List<SocketGuildUser>();

        public static void InitUsers(List<SocketGuild> guildList)
        {
            foreach (var guild in guildList)
            {
                var online = guild.DefaultChannel?.Users
                    .Where(u => u.Status == UserStatus.Online && u.IsBot == false);
                UserManager.OnlineUsers.AddRange(online);
            }

            try
            {
                using (var context = new BotContext())
                {
                    foreach (var member in UserManager.OnlineUsers)
                    {
                        var user = context.Users.AsNoTracking().FirstOrDefault(u => u.Id == member.Id && u.GuildId == member.Guild.Id);
                        if (user == null)
                        {
                            //create
                            context.Users.Add(new Users
                            {
                                Id = member.Id,
                                LastSeen = DateTime.Now,
                                Nickname = member.Nickname ?? "",
                                UserName = member.Username,
                                GuildId = member.Guild.Id
                            });
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("InitUsers: " + ex.Message);
            }
        }

        public static void Login(SocketGuildUser user)
        {
            if (user.IsBot)
                return;

            UserManager.OnlineUsers.Add(user);

            try
            {
                using (var context = new BotContext())
                {
                    if (context.Users.Any(u => u.Id == user.Id &&
                                               u.GuildId == user.Guild.Id))
                    {
                        //edit
                        var dbUser = context.Users.FirstOrDefault(u => u.Id == user.Id);
                        dbUser.LastSeen = DateTime.Now;
                        dbUser.Nickname = user.Nickname ?? "";
                        dbUser.UserName = user.Username;
                    }
                    else
                    {
                        //welcome message
                        //WelcomeEmbed(user);

                        //create
                        context.Users.Add(new Users
                        {
                            Id = user.Id,
                            LastSeen = DateTime.Now,
                            Nickname = user.Nickname ?? "",
                            UserName = user.Username,
                            GuildId = user.Guild.Id
                        });
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("LoginAsync: " + ex.Message);
            }
        }

        public static bool IsOnline(SocketGuildUser user)
        {
            return OnlineUsers.Any(u => u.Id == user.Id);
        }

        public static void Away(SocketGuildUser user)
        {
            if (user.IsBot)
                return;

            UserManager.OnlineUsers.Remove(user);

            try
            {
                using (var context = new BotContext())
                {
                    if (context.Users.Any(u => u.Id == user.Id && u.GuildId == user.Guild.Id))
                    {
                        //edit
                        var dbUser = context.Users.FirstOrDefault(u => u.Id == user.Id);
                        dbUser.LastSeen = DateTime.Now;
                        dbUser.Nickname = user.Nickname ?? "";
                        dbUser.UserName = user.Username;
                    }
                    else
                    {
                        //create
                        context.Users.Add(new Users
                        {
                            Id = user.Id,
                            LastSeen = DateTime.Now,
                            Nickname = user.Nickname ?? "",
                            UserName = user.Username,
                            GuildId = user.Guild.Id
                        });
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Away: " + ex.Message);
            }
        }

        public static string Seen(string txt)
        {
            if (UserManager.OnlineUsers.Any(u => u.Nickname == txt)) return "Now";

            using (var context = new BotContext())
            {
                var _user = context.Users.FirstOrDefault(u => string.Equals(u.Nickname, txt, StringComparison.OrdinalIgnoreCase) ||
                                                              string.Equals(u.UserName, txt, StringComparison.OrdinalIgnoreCase));

                if (_user == null)
                    return $"Never seen {txt} before.";
                else
                    return $"{txt} was last seen {_user.LastSeen.DisplayTime()}";

            }
        }
        public static string Seen(ulong id)
        {
            if (UserManager.OnlineUsers.Any(u => u.Id == id)) return "Now";

            using (var context = new BotContext())
            {
                var _user = context.Users.FirstOrDefault(u => u.Id == id);

                if (_user == null)
                    return $"Never seen <@{id}> before.";
                else
                    return $"<@{id}> was last seen {_user.LastSeen.DisplayTime()}";

            }
        }

        public static string Seen(SocketGuildUser user)
        {
            if (UserManager.OnlineUsers.Contains(user)) return "Now";

            using (var context = new BotContext())
            {
                var _user = context.Users.FirstOrDefault(u => u.Id == user.Id);

                if (_user == null)
                    return "Never";
                else
                    return $"<@{user.Id}> was last seen {_user.LastSeen.DisplayTime()}";

            }
        }

        public static void WelcomeEmbed(SocketGuildUser user)
        {
            var builder = new EmbedBuilder()
            .WithTitle($"Welcome {user.Username}!")
            .WithDescription("This appears to be your first time here! We are a very **competitive**, profesional corporation for Hades Star! Now that you know something about us, tell us something about you!")
            .WithColor(new Color(0x8D8434))
            .WithTimestamp(DateTimeOffset.Now)
            .WithFooter(footer =>
            {
                footer.WithText("Did you know we are recruiting!")
                    .WithIconUrl("https://cdn.discordapp.com/app-icons/429082137703088130/29221e4e1aa2a79332c4d9caf8472181.png");
            })
            .WithThumbnailUrl("https://cdn.discordapp.com/app-icons/429082137703088130/29221e4e1aa2a79332c4d9caf8472181.png")
            .WithAuthor(author =>
            {
                author.WithName("The Force")
                .WithIconUrl("https://cdn.discordapp.com/app-icons/429082137703088130/29221e4e1aa2a79332c4d9caf8472181.png");
            })
            .AddField("Bot Commands:", "Learn more about us.  Start by typing: ```!help```");

            var embed = builder.Build();

            Discord.UserExtensions.SendMessageAsync(user, "", false, embed);
        }
    }
}
