﻿using Discord;
using System;
using Solarbot.Data;
using Solarbot.Utilities;
using System.Collections.Generic;

namespace Solarbot.Managers
{
    public static class StatEmbedManager
    {
        public static EmbedFieldBuilder GetTrade(UserModules user)
        {
            return new EmbedFieldBuilder
            {
                Name = "**Trade**",
                Value = Calc(user,
                    new List<ShipModule>
                        {
                            ShipModule.Cargobayextension,
                            ShipModule.Shipmentcomputer,
                            ShipModule.Tradeboost,
                            ShipModule.Rush,
                            ShipModule.Tradeburst,
                            ShipModule.Shipmentdrone,
                            ShipModule.Offload,
                            ShipModule.Shipmentbeam,
                            ShipModule.Entrust,
                            ShipModule.Recall,
                        })
            };
        }
        public static EmbedFieldBuilder GetWeapons(UserModules user)
        {
            return new EmbedFieldBuilder
            {
                Name = "**Weapons**",
                Value = Calc(user,
                    new List<ShipModule>
                        {
                            ShipModule.Weakbattery,
                            ShipModule.Battery,
                            ShipModule.Laser,
                            ShipModule.Massbattery,
                            ShipModule.Duallaser,
                            ShipModule.Barrage,
                        })
            };
        }

        public static EmbedFieldBuilder GetMining(UserModules user)
        {
            return new EmbedFieldBuilder
            {
                Name = "**Mining**",
                Value = Calc(user,
                    new List<ShipModule>
                        {
                            ShipModule.Miningboost,
                            ShipModule.Hydrogenbayextension,
                            ShipModule.Enrich,
                            ShipModule.Remotemining,
                            ShipModule.Hydrogenupload,
                            ShipModule.Miningunity,
                            ShipModule.Crunch,
                            ShipModule.Genesis,
                        })
            };
        }

        public static EmbedFieldBuilder GetShields(UserModules user)
        {
            return new EmbedFieldBuilder
            {
                Name = "**Shields**",
                Value = Calc(user,
                    new List<ShipModule>
                        {
                            ShipModule.Alphashield,
                            ShipModule.Deltashield,
                            ShipModule.Passiveshield,
                            ShipModule.Omegashield,
                            ShipModule.Mirrorshield,
                            ShipModule.Blastshield,
                            ShipModule.Areashield,
                        })
            };
        }

        public static EmbedFieldBuilder GetSupport(UserModules user)
        {
            return new EmbedFieldBuilder
            {
                Name = "**Support**",
                Value = Calc(user,
                    new List<ShipModule>
                        {
                            ShipModule.Emp,
                            ShipModule.Teleport,
                            ShipModule.Redstarlifeextender,
                            ShipModule.Remoterepair,
                            ShipModule.Timewarp,
                            ShipModule.Unity,
                            ShipModule.Sanctuary,
                            ShipModule.Stealth,
                            ShipModule.Fortify,
                            ShipModule.Impulse,
                            ShipModule.Alpharocket,
                            ShipModule.Salvage,
                            ShipModule.Suppress,
                            ShipModule.Destiny,
                            ShipModule.Barrier,
                            ShipModule.Vengeance,
                            ShipModule.Deltarocket,
                            ShipModule.Leap,
                            ShipModule.Bond,
                            ShipModule.Alphadrone,
                            ShipModule.Omegarocket,
                        })
            };
        }

        public static EmbedFieldBuilder GetShips(UserModules user)
        {
            var bb = user.Battleship;
            var tx = user.Transport;
            var mine = user.Miner;

            var bbEmoji = (ShipModule)Enum.Parse(typeof(ShipModule), $"Battleship{bb}");
            var txEmoji = (ShipModule)Enum.Parse(typeof(ShipModule), $"Transport{tx}");
            var mineEmoji = (ShipModule)Enum.Parse(typeof(ShipModule), $"Miner{mine}");

            var value = "";
            value += bbEmoji.ToCode() + $"***{bb} ***";
            value += txEmoji.ToCode() + $"***{tx} ***";
            value += mineEmoji.ToCode() + $"***{mine} ***";

            return new EmbedFieldBuilder
            {
                Name = "Ships",
                Value = value
            };
        }

        public static string Calc(UserModules user, List<ShipModule> list)
        {
            string value = "";
            int index = 0;
            foreach (var mod in list)
            { 
                value = value + mod.ToCode() + $"***{user.GetStat(mod)} ***";
                index++;

                if (index % 4 == 0)
                    value = value + "\n";
            }
            return value;
        }
    }


}
