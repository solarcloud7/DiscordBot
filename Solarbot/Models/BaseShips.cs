﻿using System;
using Solarbot.Data;
using Solarbot.Models.Ships;
using Solarbot.Exceptions;
using System.Collections.Generic;

namespace Solarbot.Models
{
    public abstract class BaseShips
    {
        public uint Level;
        protected Users user;

        public BaseShips(uint Level = 0)
        {
            this.Level = Level;
        }

        /**
         * Get the user-friendly name for this module
         */
        public abstract string GetName();

        /**
         * Get some useful information about this module and its level
         */
        public abstract string GetInfo();

        /**
         * Get a list of equipped modules on this ship
         */
        public abstract List<EquipmentModules> GetEquippedModules();

        /**
         * Get the total number of available Support modules on this given ship
         */
        public abstract uint GetSupportModuleSlots();

        public string GetOwner()
        {
            return this.user.DisplayName;
        }

        /**
         * Set the level of this module.  If a User is attached already, their database is updated on-the-fly.
         */
        public abstract void SetLevel(uint Level);

        /**
         * Set the modules for this ship, the specifics and list are handled internally.
         */
        public abstract void SetModules(string[] modules);

        /**
         * Clears the support modules for this ship.
         */
        public abstract void ClearModules();

        /**
         * Attach a given User onto this module, useful for updating their level.
         */
        public abstract void Fromuser(Users user);

        /**
        * Resolve a user-supplied ship name to its fully resolved version
        */
        public static string ResolveName(string shipname)
        {
            var matches = new List<string>();
            shipname = shipname.ToLower();

            // Provide a lookup table of module name to allow the user to query a module based on its shortest possible name.
            // ex: "cargobayextension" can be requested with just simply "car"
            string[] names = { "battleship", "transport", "miner" };

            foreach (var n in names)
            {
                if (n.StartsWith(shipname))
                {
                    matches.Add(n);
                }
            }

            if (matches.Count > 1)
            {
                throw new UserException(
                    "Ambigious ship name requested, [" + shipname + "]",
                    "Please clarify what ship you mean as your query matched multiple possibilities, (" + String.Join(", ", matches.ToArray()) + ")"
                );
            }
            else if (matches.Count == 0)
            {
                throw new UserException(
                    "Unknown ship requested, [" + shipname + "]",
                    "Sorry, but I'm not sure what ship you're referring to."
                );
            }
            else
            {
                return matches[0];
            }
        }

        /**
         * Get a ship from its name, or an acceptable user alias/shortname
         */
        public static BaseShips ShipFromName(string shipname)
        {
            switch (BaseShips.ResolveName(shipname))
            {
                case "battleship":
                    return new BattleShip();
                case "transport":
                    return new TransportShip();
                case "miner":
                    throw new UserException("BaseShips.cs ShipFromName()","Miner NOT implemented yet");
                default:
                    throw new UserException("Unknown ship requested, [" + shipname + "]", "Sorry, but I'm not sure what ship you were referring to, please try to be more specific.");
            }
        }

        /**
         * Simple function to convert a full uint stat to a slightly shorter version with its numeric suffix.
         */
        public static string StatFormatter(uint stat)
        {
            if (stat >= 1000)
            {
                uint sM = stat / 1000;
                uint sm = (stat - (sM * 1000) ) / 100;

                return sM.ToString() + (sm > 0 ? "." + sm.ToString() : "") + "k";
            }
            else
            {
                return stat.ToString();
            }
        }
    }
}
