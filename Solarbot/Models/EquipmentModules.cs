﻿using System;
using Solarbot.Data;
using Solarbot.Models.Modules.Trade;
using Solarbot.Models.Modules.Support;
using Solarbot.Models.Modules.Weapons;
using Solarbot.Models.Modules.Shields;
using Solarbot.Exceptions;
using System.Collections.Generic;

namespace Solarbot.Models
{
    public abstract class EquipmentModules
    {
        public string KeyName;
        public string Type;
        public uint Level;
        protected Users user;

        public static string[] ModuleNames = { 
            "cargobayextension", 
            "emp", "teleport", "repair", "timewarp", "unity", "sanctuary", "stealth", "fortify", "impulse", "alpharocket", "salvage", "suppress", "destiny", "barrier", "vengeance", "leap", "alphadrone",
            "battery", "laser", "massbattery", "duallaser", "barrage",
            "alphashield", "deltashield", "passiveshield", "omegashield", "mirrorshield", "areashield"
        };
        public static string TypeSupport = "support";
        public static string TypeTrade = "trade";
        public static string TypeMining = "mining";
        public static string TypeWeapon = "weapon";
        public static string TypeShield = "shield";

        public EquipmentModules(uint Level = 0)
        {
            this.Level = Level;
        }

        /**
         * Get the user-friendly name for this module
         */
        public abstract string GetName();

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public abstract string GetShortName();

        /**
         * Get some useful information about this module and its level
         */
        public abstract string GetInfo();

        /**
         * Set the level of this module.  If a User is attached already, their database is updated on-the-fly.
         */
        public abstract void SetLevel(uint Level);

        /**
         * Attach a given User onto this module, useful for updating their level.
         */
        public abstract void Fromuser(Users user);

        /**
         * Hook handler for allowing a module to update the metrics for the ship it was attached to.
         */
        public abstract void AddedToShip(BaseShips ship);

        /**
         * Resolve a user-supplied module name to its fully resolved version
         */
        public static string ResolveName(string modname)
        {
            var matches = new List<string>();
            modname = modname.ToLower();

            // Provide a lookup table of module name to allow the user to query a module based on its shortest possible name.
            // ex: "cargobayextension" can be requested with just simply "car"
            foreach (var n in EquipmentModules.ModuleNames)
            {
                if (n.StartsWith(modname))
                {
                    matches.Add(n);
                }
            }

            if (matches.Count > 1)
            {
                throw new UserException(
                    "Ambigious module name requested, [" + modname + "]",
                    "Please clarify what module you mean as your query matched multiple possibilities, (" + String.Join(", ", matches.ToArray()) + ")"
                );
            }
            else if (matches.Count == 0)
            {
                throw new UserException(
                    "Unknown module requested, [" + modname + "]",
                    "Sorry, but I'm not sure what module you're referring to."
                );
            }
            else
            {
                // Exactly 1 match was located, YAY!
                return matches[0];
            }
        }

        public static EquipmentModules EquipmentFromName(string modname)
        {
            switch (EquipmentModules.ResolveName(modname))
            {
                case "alphadrone":
                    return new AlphaDroneModule();
                case "alpharocket":
                    return new AlphaRocketModule();
                case "alphashield":
                    return new AlphaShieldModule();
                case "areashield":
                    return new AreaShieldModule();
                case "barrier":
                    return new BarrierModule();
                case "battery":
                    return new BatteryModule();
                case "barrage":
                    return new BarrageModule();
                case "cargobayextension":
                    return new CargoModule();
                case "deltashield":
                    return new DeltaShieldModule();
                case "destiny":
                    return new DestinyModule();
                case "duallaser":
                    return new DualLaserModule();
                case "emp":
                    return new EMPModule();
                case "fortify":
                    return new FortifyModule();
                case "impulse":
                    return new ImpulseModule();
                case "laser":
                    return new LaserModule();
                case "leap":
                    return new LeapModule();
                case "massbattery":
                    return new MassBatteryModule();
                case "mirrorshield":
                    return new MirrorShieldModule();
                case "omegashield":
                    return new OmegaShieldModule();
                case "passiveshield":
                    return new PassiveShieldModule();
                case "salvage":
                    return new SalvageModule();
                case "sanctuary":
                    return new SanctuaryModule();
                case "stealth":
                    return new StealthModule();
                case "suppress":
                    return new SuppressModule();
                case "teleport":
                    return new TeleportModule();
                case "timewarp":
                    return new TimeModule();
                case "repair":
                    return new RemoteRepairModule();
                case "unity":
                    return new UnityModule();
                case "vengeance":
                    return new VengeanceModule();
                default:
                    throw new UserException("Unknown module requested, [" + modname + "]", "Sorry, but I'm not sure what module you were referring to, please try to be more specific.");
            }
        }
    }
}
