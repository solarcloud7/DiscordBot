﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Modules.Trade;
using System.Collections.Generic;

namespace Solarbot.Models.Ships
{
    public class BattleShip : BaseShips
    {
        private EquipmentModules weapon;
        private EquipmentModules shield;
        private EquipmentModules module1;
        private EquipmentModules module2;
        private EquipmentModules module3;
        private EquipmentModules module4;

        public uint DphMin = 480;
        public uint DphMax = 0;
        public uint DpsModifier = 0;
        public uint Shield = 0;
        public uint Targets = 1;

        public BattleShip(uint Level = 0) : base(Level)
        {

        }

        public override string GetName()
        {
            return "Battleship";
        }

        public string GetDamage()
        {
            string damage = StatFormatter(this.DphMin);
            uint dMax = this.DphMax;

            if (dMax == 0)
            {
                // Set them to the same if it's not set otherwise, makes the below logic cleaner.
                dMax = this.DphMin;
            }

            if (this.DpsModifier > 1)
            {
                // Some modules may increase the max damage of this weapon.
                dMax *= this.DpsModifier;
            }

            if (dMax != this.DphMin)
            {
                // The max and min damages are different, update the damage string to include max damage!
                damage += " - " + StatFormatter(dMax);
            }

            if (this.Targets > 1)
            {
                // Some modules allow a battleship to target multiple ships, include this in the damage report too!
                damage += " x" + this.Targets.ToString();
            }

            return damage;
        }

        public override string GetInfo()
        {
            return this.GetDamage() + " D, " + StatFormatter(this.GetHullStrength()) + " H, " + StatFormatter(this.Shield) + " S";
        }

        public uint GetHullStrength()
        {
            switch (this.Level)
            {
                case 1:
                    return 4000;
                case 2:
                    return 5000;
                case 3:
                    return 6000;
                case 4:
                    return 7500;
                case 5:
                    return 9000;
                default:
                    return 0;
            }
        }

        /**
         * Get a list of equipped modules on this ship
         */
        public override List<EquipmentModules> GetEquippedModules()
        {
            var modules = new List<EquipmentModules>();
            if (this.weapon != null)
            {
                modules.Add(this.weapon);
            }

            if (this.shield != null)
            {
                modules.Add(this.shield);
            }

            if (this.module1 != null)
            {
                modules.Add(this.module1);
            }

            if (this.module2 != null)
            {
                modules.Add(this.module2);
            }

            if (this.module3 != null)
            {
                modules.Add(this.module3);
            }

            if (this.module4 != null)
            {
                modules.Add(this.module4);
            }

            return modules;
        }

        public override uint GetSupportModuleSlots()
        {
            // Battleship support module count is currently [level] - 1.
            return (uint)(this.Level == 0 ? 0 : this.Level - 1);
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 5)
            {
                throw new UserException("Invalid ship level requested", "PSH, sure.... the max level for a battleship is 5.  *wiseguy*");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ShipBattleship = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ShipBattleship = this.Level;
                    context.SaveChanges();
                }
            }
        }

        public override void ClearModules()
        {
            using (var context = new BotContext())
            {
                var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                u.EquippedBattleshipSupport1 = "";
                u.EquippedBattleshipSupport2 = "";
                u.EquippedBattleshipSupport3 = "";
                u.EquippedBattleshipSupport4 = "";

                context.SaveChanges();
            }
        }

        /**
         * Set the modules for this ship, the specifics and list are handled internally.
         */
        public override void SetModules(string[] modules)
        {
            using (var context = new BotContext())
            {
                var modCounter = 1;
                uint maxSupport = this.GetSupportModuleSlots();
                var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);

                if (modules.Length == 1)
                {
                    // Single module set, allow an individual module to be set which appends to the list,
                    // or throw an exception if the list of the module type is already full.
                    EquipmentModules mod = EquipmentModules.EquipmentFromName(modules[0]);

                    if (mod.Type == EquipmentModules.TypeShield)
                    {
                        u.EquippedBattleshipShield = mod.KeyName;
                    }

                    if (mod.Type == EquipmentModules.TypeWeapon)
                    {
                        u.EquippedBattleshipWeapon = mod.KeyName;
                    }

                    if (mod.Type == EquipmentModules.TypeSupport)
                    {
                        if (maxSupport >= 1 && u.EquippedBattleshipSupport1 == "")
                        {
                            u.EquippedBattleshipSupport1 = mod.KeyName;
                        }
                        else if (maxSupport >= 2 && u.EquippedBattleshipSupport2 == "")
                        {
                            u.EquippedBattleshipSupport2 = mod.KeyName;
                        }
                        else if (maxSupport >= 3 && u.EquippedBattleshipSupport3 == "")
                        {
                            u.EquippedBattleshipSupport3 = mod.KeyName;
                        }
                        else if (maxSupport >= 4 && u.EquippedBattleshipSupport4 == "")
                        {
                            u.EquippedBattleshipSupport4 = mod.KeyName;
                        }
                    }
                }
                else
                {
                    // Multi module set, allow multiple modules to be set in order.
                    foreach (string modname in modules)
                    {
                        EquipmentModules mod = EquipmentModules.EquipmentFromName(modname);

                        if (mod.Type == EquipmentModules.TypeShield)
                        {
                            u.EquippedBattleshipShield = mod.KeyName;
                        }

                        if (mod.Type == EquipmentModules.TypeWeapon)
                        {
                            u.EquippedBattleshipWeapon = mod.KeyName;
                        }

                        if (mod.Type == EquipmentModules.TypeSupport && modCounter <= maxSupport)
                        {
                            switch (modCounter)
                            {
                                case 1:
                                    u.EquippedBattleshipSupport1 = mod.KeyName;
                                    break;
                                case 2:
                                    u.EquippedBattleshipSupport2 = mod.KeyName;
                                    break;
                                case 3:
                                    u.EquippedBattleshipSupport3 = mod.KeyName;
                                    break;
                                case 4:
                                    u.EquippedBattleshipSupport4 = mod.KeyName;
                                    break;
                            }

                            modCounter++;
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ShipBattleship;

            uint maxMods = this.GetSupportModuleSlots();

            // Load the various equipment that's set on this battleship
            if (user.EquippedBattleshipShield != "")
            {
                this.shield = user.GetEquipmentModule(user.EquippedBattleshipShield);
                this.shield.AddedToShip(this);
            }

            if (user.EquippedBattleshipWeapon != "")
            {
                this.weapon = user.GetEquipmentModule(user.EquippedBattleshipWeapon);
                this.weapon.AddedToShip(this);
            }

            if (maxMods >= 1 && user.EquippedBattleshipSupport1 != "")
            {
                this.module1 = user.GetEquipmentModule(user.EquippedBattleshipSupport1);
                this.module1.AddedToShip(this);
            }

            if (maxMods >= 2 && user.EquippedBattleshipSupport2 != "")
            {
                this.module2 = user.GetEquipmentModule(user.EquippedBattleshipSupport2);
                this.module2.AddedToShip(this);
            }

            if (maxMods >= 3 && user.EquippedBattleshipSupport3 != "")
            {
                this.module3 = user.GetEquipmentModule(user.EquippedBattleshipSupport3);
                this.module3.AddedToShip(this);
            }

            if (maxMods >= 4 && user.EquippedBattleshipSupport4 != "")
            {
                this.module4 = user.GetEquipmentModule(user.EquippedBattleshipSupport4);
                this.module4.AddedToShip(this);
            }
        }
    }
}
