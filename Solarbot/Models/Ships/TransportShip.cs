﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Modules.Trade;
using System.Collections.Generic;

namespace Solarbot.Models.Ships
{
    public class TransportShip : BaseShips
    {
        private CargoModule cargo;
        private EquipmentModules module1;

        public TransportShip(uint Level = 0) : base(Level)
        {
            
        }

        public override string GetName()
        {
            return "Transport";
        }

        public override string GetInfo()
        {
            string info = "";
            uint slots = this.GetBaseCargoSlots();

            if (this.cargo != null)
            {
                slots += this.cargo.GetAdditionalSlots();
                info += slots.ToString() + " slots w/" + this.cargo.GetShortName();
            }
            else
            {
                info += slots.ToString() + " slots";
            }

            return info;
        }

        /**
         * Get a list of equipped modules on this ship
         */
        public override List<EquipmentModules> GetEquippedModules()
        {
            var modules = new List<EquipmentModules>();

            // Transports can only have 1.
            if (this.module1 != null)
            {
                modules.Add(this.module1);
            }

            return modules;
        }

        public uint GetBaseCargoSlots()
        {
            // This is linked directly to the level at current.
            return this.Level;
        }

        public override uint GetSupportModuleSlots()
        {
            return this.Level >= 3 ? (uint)1 : (uint)0;
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 5)
            {
                throw new UserException("Invalid ship level requested", "PSH, sure.... the max level for transports is 5.  *wiseguy*");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ShipTransport = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ShipTransport = this.Level;
                    context.SaveChanges();
                }
            }
        }

        /**
         * Clear the modules for this ship, the specifics and list are handled internally.
         */
        public override void ClearModules()
        {
            using (var context = new BotContext())
            {
                var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                u.EquippedTransportSupport1 = "";
                context.SaveChanges();
            }
        }

        /**
         * Set the modules for this ship, the specifics and list are handled internally.
         */
        public override void SetModules(string[] modules)
        {
            string modname;

            if (modules.Length > this.GetSupportModuleSlots())
            {
                throw new UserException("Too many modules requested to be set", this.GetName() + " only supports " + this.GetSupportModuleSlots() + " modules!");
            }

            if (modules.Length == 0)
            {
                // Just clear them.
                modname = "";
            }
            else
            {
                EquipmentModules mod = EquipmentModules.EquipmentFromName(modules[0]);

                if (mod.Type != EquipmentModules.TypeSupport)
                {
                    throw new UserException("Invalid module type requested for transports", "Sorry, but transports only have support-type modules!");
                }

                modname = mod.KeyName;
            }

            using (var context = new BotContext())
            {
                var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                u.EquippedTransportSupport1 = modname;
                context.SaveChanges();
            }
        }

        public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ShipTransport;

            // If this user has Cargo Bay enabled, assume they are using it.
            // Transports don't make much sense if they don't have a cargo bay equipped.
            CargoModule c = (CargoModule)user.GetEquipmentModule("cargobayextension");
            if (c.Level > 0)
            {
                this.cargo = c;
            }

            if (this.GetSupportModuleSlots() > 0)
            {
                // This ship supports a module slot!  Check and see if the user has set one.
                if (user.EquippedTransportSupport1 != "")
                {
                    this.module1 = user.GetEquipmentModule(user.EquippedTransportSupport1);
                }
            }
        }
    }
}
