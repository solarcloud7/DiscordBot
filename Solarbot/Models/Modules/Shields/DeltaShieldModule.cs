﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Shields
{
    public class DeltaShieldModule : EquipmentModules
    {
        public DeltaShieldModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeShield;
            this.KeyName = "deltashield";
        }

        public override string GetName()
        {
            return "Delta Shield";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " delta";
        }

        public override string GetInfo()
        {
            return this.GetStrength().ToString() + " Shield Strength and +" + this.GetSpeedIncrease() + " Speed";
        }

        public uint GetStrength()
        {
            switch (this.Level)
            {
                case 1:
                    return 3500;
                case 2:
                    return 3800;
                case 3:
                    return 4100;
                case 4:
                    return 4400;
                case 5:
                    return 4700;
                case 6:
                    return 5000;
                case 7:
                    return 5300;
                case 8:
                    return 5600;
                case 9:
                    return 5900;
                case 10:
                    return 6200;
                default:
                    return 0;
            }
        }

        public string GetSpeedIncrease()
        {
            switch (this.Level)
            {
                case 1:
                    return "6%";
                case 2:
                    return "10%";
                case 3:
                    return "14%";
                case 4:
                    return "18%";
                case 5:
                    return "22%";
                case 6:
                    return "26%";
                case 7:
                    return "30%";
                case 8:
                    return "33%";
                case 9:
                    return "36%";
                case 10:
                    return "40%";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleShieldDelta = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleShieldDelta = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).Shield = this.GetStrength();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleShieldDelta;
        }
    }
}
