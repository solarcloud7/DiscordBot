﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Shields
{
    public class AlphaShieldModule : EquipmentModules
    {
        public AlphaShieldModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeShield;
            this.KeyName = "alphashield";
        }

        public override string GetName()
        {
            return "Alpha Shield";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " alpha";
        }

        public override string GetInfo()
        {
            return this.GetStrength().ToString() + " Shield Strength";
        }

        public uint GetStrength()
        {
            switch (this.Level)
            {
                case 1:
                    return 2000;
                case 2:
                    return 2300;
                case 3:
                    return 2600;
                case 4:
                    return 2900;
                case 5:
                    return 3200;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 5)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleShieldAlpha = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleShieldAlpha = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).Shield = this.GetStrength();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleShieldAlpha;
        }
    }
}
