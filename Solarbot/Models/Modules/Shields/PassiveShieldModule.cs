﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Shields
{
    public class PassiveShieldModule : EquipmentModules
    {
        public PassiveShieldModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeShield;
            this.KeyName = "passiveshield";
        }

        public override string GetName()
        {
            return "Passive Shield";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " passv";
        }

        public override string GetInfo()
        {
            return this.GetStrength().ToString() + " Shield Strength";
        }

        public uint GetStrength()
        {
            switch (this.Level)
            {
                case 1:
                    return 5000;
                case 2:
                    return 6000;
                case 3:
                    return 7000;
                case 4:
                    return 8000;
                case 5:
                    return 9000;
                case 6:
                    return 10000;
                case 7:
                    return 11500;
                case 8:
                    return 13000;
                case 9:
                    return 14500;
                case 10:
                    return 16000;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleShieldPassive = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleShieldPassive = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).Shield = this.GetStrength();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleShieldPassive;
        }
    }
}
