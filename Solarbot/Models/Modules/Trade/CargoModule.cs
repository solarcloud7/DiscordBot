﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;

namespace Solarbot.Models.Modules.Trade
{
    public class CargoModule : EquipmentModules
    {
        public CargoModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeTrade;
            this.KeyName = "cargobayextension";
        }

        public override string GetName()
        {
            return "Cargo Bay Extension";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " cargo";
        }

        public override string GetInfo()
        {
            return "+" + this.GetAdditionalSlots().ToString() + " cargo slots";
        }

        public uint GetAdditionalSlots()
        {
            switch (this.Level)
            {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 5;
                case 5:
                    return 7;
                case 6:
                    return 9;
                case 7:
                    return 12;
                case 8:
                    return 15;
                case 9:
                    return 18;
                case 10:
                    return 22;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleTradeCargo = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleTradeCargo = this.Level;
                    context.SaveChanges();
                }
            }
        }

        public override void AddedToShip(BaseShips ship)
        {
        }

        public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleTradeCargo;
        }
    }
}
