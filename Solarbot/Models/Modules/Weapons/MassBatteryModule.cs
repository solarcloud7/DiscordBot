﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Weapons
{
    public class MassBatteryModule : EquipmentModules
    {
        public MassBatteryModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeWeapon;
            this.KeyName = "massbattery";
        }

        public override string GetName()
        {
            return "Mass Battery";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " masbt";
        }

        public override string GetInfo()
        {
            return this.GetDPH().ToString() + " damage per hour";
        }

        public uint GetDPH()
        {
            switch (this.Level)
            {
                case 1:
                    return 360;
                case 2:
                    return 420;
                case 3:
                    return 480;
                case 4:
                    return 540;
                case 5:
                    return 540;
                case 6:
                    return 600;
                case 7:
                    return 660;
                case 8:
                    return 660;
                case 9:
                    return 720;
                case 10:
                    return 780;
                default:
                    return 0;
            }
        }

        public uint GetTargets()
        {
            switch (this.Level)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                    return 3;
                case 5:
                case 6:
                case 7:
                    return 4;
                case 8:
                case 9:
                case 10:
                    return 5;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleWeaponMassBattery = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleWeaponMassBattery = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
        {
            ((BattleShip)ship).DphMin = this.GetDPH();
            ((BattleShip)ship).Targets = this.GetTargets();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleWeaponMassBattery;
        }
    }
}
