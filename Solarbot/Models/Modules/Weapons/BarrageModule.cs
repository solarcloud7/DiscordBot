﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Weapons
{
    public class BarrageModule : EquipmentModules
    {
        public BarrageModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeWeapon;
            this.KeyName = "barrage";
        }

        public override string GetName()
        {
            return "Barrage";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " barag";
        }

        public override string GetInfo()
        {
            return this.GetDPHMin().ToString() + " - " + this.GetDPHMax().ToString() + " damage per hour";
        }

        public uint GetDPHMin()
        {
            switch (this.Level)
            {
                case 1:
                    return 480;
                case 2:
                    return 540;
                case 3:
                    return 1;
                case 4:
                    return 1;
                case 5:
                    return 1;
                case 6:
                    return 1;
                case 7:
                    return 1;
                case 8:
                    return 1;
                case 9:
                    return 1;
                case 10:
                    return 1;
                default:
                    return 0;
            }
        }

        public uint GetDPHMax()
        {
            switch (this.Level)
            {
                case 1:
                    return 2280;
                case 2:
                    return 2520;
                case 3:
                    return 2;
                case 4:
                    return 2;
                case 5:
                    return 2;
                case 6:
                    return 2;
                case 7:
                    return 2;
                case 8:
                    return 2;
                case 9:
                    return 2;
                case 10:
                    return 2;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleWeaponBarrage = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleWeaponBarrage = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).DphMin = this.GetDPHMin();
            ((BattleShip)ship).DphMax = this.GetDPHMax();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleWeaponBarrage;
        }
    }
}
