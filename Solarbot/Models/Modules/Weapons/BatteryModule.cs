﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Weapons
{
    public class BatteryModule : EquipmentModules
    {
        public BatteryModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeWeapon;
            this.KeyName = "battery";
        }

        public override string GetName()
        {
            return "Battery";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " batry";
        }

        public override string GetInfo()
        {
            return this.GetDPH().ToString() + " damage per hour";
        }

        public uint GetDPH()
        {
            switch (this.Level)
            {
                case 1:
                    return 600;
                case 2:
                    return 720;
                case 3:
                    return 840;
                case 4:
                    return 960;
                case 5:
                    return 1080;
                case 6:
                    return 1260;
                case 7:
                    return 1500;
                case 8:
                    return 1800;
                case 9:
                    return 2100;
                case 10:
                    return 2400;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleWeaponBattery = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleWeaponBattery = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).DphMin = this.GetDPH();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleWeaponBattery;
        }
    }
}
