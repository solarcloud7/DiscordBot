﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Weapons
{
    public class LaserModule : EquipmentModules
    {
        public LaserModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeWeapon;
            this.KeyName = "laser";
        }

        public override string GetName()
        {
            return "Laser";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " laser";
        }

        public override string GetInfo()
        {
            return this.GetDPHMin().ToString() + " - " + this.GetDPHMax().ToString() + " damage per hour";
        }

        public uint GetDPHMin()
        {
            switch (this.Level)
            {
                case 1:
                    return 480;
                case 2:
                    return 540;
                case 3:
                    return 600;
                case 4:
                    return 660;
                case 5:
                    return 750;
                case 6:
                    return 840;
                case 7:
                    return 930;
                case 8:
                    return 1020;
                case 9:
                    return 1100;
                case 10:
                    return 1200;
                default:
                    return 0;
            }
        }

        public uint GetDPHMax()
        {
            switch (this.Level)
            {
                case 1:
                    return 1200;
                case 2:
                    return 1320;
                case 3:
                    return 1440;
                case 4:
                    return 1620;
                case 5:
                    return 1800;
                case 6:
                    return 2040;
                case 7:
                    return 2300;
                case 8:
                    return 2600;
                case 9:
                    return 2800;
                case 10:
                    return 3000;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleWeaponLaser = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleWeaponLaser = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).DphMin = this.GetDPHMin();
            ((BattleShip)ship).DphMax = this.GetDPHMax();
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleWeaponLaser;
        }
    }
}
