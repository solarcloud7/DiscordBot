﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;

namespace Solarbot.Models.Modules.Support
{
    public class RemoteRepairModule : EquipmentModules
    {
        public RemoteRepairModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "repair";
        }

        public override string GetName()
        {
            return "Remote Repair";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " repair";
        }

        public override string GetInfo()
        {
            return this.GetRange() + " Range for " + this.GetDuration();
        }

        public string GetRange()
        {
            switch (this.Level)
            {
                case 1:
                    return "40AU";
                case 2:
                    return "50AU";
                case 3:
                    return "60AU";
                case 4:
                    return "70AU";
                case 5:
                    return "80AU";
                case 6:
                    return "90AU";
                case 7:
                    return "110AU";
                case 8:
                    return "130AU";
                case 9:
                    return "150AU";
                case 10:
                    return "170AU";
                default:
                    return "";
            }
        }

        public string GetDuration()
        {
            switch (this.Level)
            {
                case 1:
                    return "5h";
                case 2:
                    return "@TODO";
                case 3:
                    return "@TODO";
                case 4:
                    return "@TODO";
                case 5:
                    return "@TODO";
                case 6:
                    return "@TODO";
                case 7:
                    return "@TODO";
                case 8:
                    return "@TODO";
                case 9:
                    return "@TODO";
                case 10:
                    return "@TODO";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportRepair = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportRepair = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportRepair;
        }
    }
}
