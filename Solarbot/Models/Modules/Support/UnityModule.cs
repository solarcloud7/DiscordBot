﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class UnityModule : EquipmentModules
    {
        public UnityModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "unity";
        }

        public override string GetName()
        {
            return "Unity";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " unity";
        }

        public override string GetInfo()
        {
            return this.GetDamageIncrease() + " Damage Increase per Ally";
        }

        public string GetDamageIncrease()
        {
            switch (this.Level)
            {
                case 1:
                    return "18%";
                case 2:
                    return "20%";
                case 3:
                    return "22%";
                case 4:
                    return "24%";
                case 5:
                    return "26%";
                case 6:
                    return "28%";
                case 7:
                    return "30%";
                case 8:
                    return "33%";
                case 9:
                    return "36%";
                case 10:
                    return "40%";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportUnity = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportUnity = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
            ((BattleShip)ship).DpsModifier = 2;
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportUnity;
        }
    }
}
