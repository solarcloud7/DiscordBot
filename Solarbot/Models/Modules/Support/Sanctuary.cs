﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class SanctuaryModule : EquipmentModules
    {
        public SanctuaryModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "sanctuary";
        }

        public override string GetName()
        {
            return "Sanctuary";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "sanctuary";
        }

        public override string GetInfo()
        {
            return "";
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 1)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = 1;
        }
    }
}
