﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class SalvageModule : EquipmentModules
    {
        public SalvageModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "salvage";
        }

        public override string GetName()
        {
            return "Salvage";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " slvge";
        }

        public override string GetInfo()
        {
            return this.GetDamageReduction() + " Hull Repaired per Ship";
        }

        public string GetDamageReduction()
        {
            switch (this.Level)
            {
                case 1:
                    return "10%";
                case 2:
                    return "13%";
                case 3:
                    return "16%";
                case 4:
                    return "18%";
                case 5:
                    return "20%";
                case 6:
                    return "21%";
                case 7:
                    return "22%";
                case 8:
                    return "23%";
                case 9:
                    return "24%";
                case 10:
                    return "25%";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportSalvage = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportSalvage = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportSalvage;
        }
    }
}
