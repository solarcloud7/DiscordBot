﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;

namespace Solarbot.Models.Modules.Support
{
    public class EMPModule : EquipmentModules
    {
        public EMPModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "emp";
        }

        public override string GetName()
        {
            return "EMP";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " emp";
        }

        public override string GetInfo()
        {
            return this.GetDuration() + " Stun Duration";
        }

        public string GetDuration()
        {
            switch (this.Level)
            {
                case 1:
                    return "30s";
                case 2:
                    return "33s";
                case 3:
                    return "36s";
                case 4:
                    return "6h 20m";
                case 5:
                    return "6h 40m";
                case 6:
                    return "42s";
                case 7:
                    return "44s";
                case 8:
                    return "46s";
                case 9:
                    return "48s";
                case 10:
                    return "50s";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportEmp = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportEmp = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportEmp;
        }
    }
}
