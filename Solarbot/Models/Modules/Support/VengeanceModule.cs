﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class VengeanceModule : EquipmentModules
    {
        public VengeanceModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "vengeance";
        }

        public override string GetName()
        {
            return "Vengeance";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " vngce";
        }

        public override string GetInfo()
        {
            return this.GetDamage().ToString() + " Damage Explosion";
        }

        public uint GetDamage()
        {
            switch (this.Level)
            {
                case 1:
                    return 2200;
                case 2:
                    return 2750;
                case 3:
                    return 3410;
                case 4:
                    return 4400;
                case 5:
                    return 5500;
                case 6:
                    return 6600;
                case 7:
                    return 8250;
                case 8:
                    return 10450;
                case 9:
                    return 13200;
                case 10:
                    return 16500;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportVengeance = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportVengeance = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportVengeance;
        }
    }
}
