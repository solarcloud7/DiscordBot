﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;

namespace Solarbot.Models.Modules.Support
{
    public class TimeModule : EquipmentModules
    {
        public TimeModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "timewarp";
        }

        public override string GetName()
        {
            return "Time Warp";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " twarp";
        }

        public override string GetInfo()
        {
            return this.GetTimeFactor() + " Time Factor";
        }

        public string GetTimeFactor()
        {
            switch (this.Level)
            {
                case 1:
                    return "x1.20";
                case 2:
                    return "x1.30";
                case 3:
                    return "x1.40";
                case 4:
                    return "x1.50";
                case 5:
                    return "x1.60";
                case 6:
                    return "x1.70";
                case 7:
                    return "x1.80";
                case 8:
                    return "x1.90";
                case 9:
                    return "x2.00";
                case 10:
                    return "x2.10";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportTime = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportTime = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportTime;
        }
    }
}
