﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class AlphaRocketModule : EquipmentModules
    {
        public AlphaRocketModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "alpharocket";
        }

        public override string GetName()
        {
            return "Alpha Rocket";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " rockt";
        }

        public override string GetInfo()
        {
            return this.GetDamage().ToString() + " Damage Explosion";
        }

        public uint GetDamage()
        {
            switch (this.Level)
            {
                case 1:
                    return 1500;
                case 2:
                    return 1900;
                case 3:
                    return 2400;
                case 4:
                    return 3000;
                case 5:
                    return 3900;
                case 6:
                    return 5000;
                case 7:
                    return 6200;
                case 8:
                    return 8000;
                case 9:
                    return 0;
                case 10:
                    return 0;
                default:
                    return 0;
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 8)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportRocket = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportRocket = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportRocket;
        }
    }
}
