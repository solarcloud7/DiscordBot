﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;
using Solarbot.Models.Ships;

namespace Solarbot.Models.Modules.Support
{
    public class ImpulseModule : EquipmentModules
    {
        public ImpulseModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "impulse";
        }

        public override string GetName()
        {
            return "Impulse";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " impls";
        }

        public override string GetInfo()
        {
            return this.GetDuration() + " 4x Speed Duration";
        }

        public string GetDuration()
        {
            switch (this.Level)
            {
                case 1:
                    return "30m";
                case 2:
                    return "@TODO";
                case 3:
                    return "@TODO";
                case 4:
                    return "@TODO";
                case 5:
                    return "@TODO";
                case 6:
                    return "@TODO";
                case 7:
                    return "@TODO";
                case 8:
                    return "@TODO";
                case 9:
                    return "@TODO";
                case 10:
                    return "@TODO";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportImpulse = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportImpulse = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportImpulse;
        }
    }
}
