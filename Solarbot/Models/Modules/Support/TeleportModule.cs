﻿using System;
using Solarbot.Models;
using Solarbot.Data;
using System.Linq;
using Solarbot.Exceptions;

namespace Solarbot.Models.Modules.Support
{
    public class TeleportModule : EquipmentModules
    {
        public TeleportModule(uint Level = 0) : base(Level) {
            this.Type = EquipmentModules.TypeSupport;
            this.KeyName = "teleport";
        }

        public override string GetName()
        {
            return "Teleport";
        }

        /**
         * Get the short name for this module, useful in tables and merging with other data.
         */
        public override string GetShortName()
        {
            return "lv " + this.Level.ToString() + " tele";
        }

        public override string GetInfo()
        {
            return this.GetRange() + " Jump Range";
        }

        public string GetRange()
        {
            switch (this.Level)
            {
                case 1:
                    return "300AU";
                case 2:
                    return "330AU";
                case 3:
                    return "360AU";
                case 4:
                    return "400AU";
                case 5:
                    return "440AU";
                case 6:
                    return "485AU";
                case 7:
                    return "535AU";
                case 8:
                    return "580AU";
                case 9:
                    return "650AU";
                case 10:
                    return "700AU";
                default:
                    return "";
            }
        }

        public override void SetLevel(uint Level)
        {
            if (Level > 10)
            {
                throw new UserException("Too high of level requested", "Your module is **OVER 9000**!  eh?... sure it is.");
            }

            this.Level = Level;
            if (this.user != null)
            {
                this.user.ModuleSupportTeleport = this.Level;

                using (var context = new BotContext())
                {
                    var u = context.Users.FirstOrDefault(m => m.Id == this.user.Id && m.GuildId == this.user.GuildId);
                    u.ModuleSupportTeleport = this.Level;
                    context.SaveChanges();
                }
            }
        }

		public override void AddedToShip(BaseShips ship)
		{
		}

		public override void Fromuser(Users user)
        {
            this.user = user;
            this.Level = user.ModuleSupportTeleport;
        }
    }
}
