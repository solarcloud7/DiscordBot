﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solarbot.Models
{
    public class AppConfig
    {
        public static string BotToken { get; set; }
        public static string DebugBotToken { get; set; }
        public static string TwilioAccountSid { get; set; }
        public static string TwilioAuthToken { get; set; }
    }
}
