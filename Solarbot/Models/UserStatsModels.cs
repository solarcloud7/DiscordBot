﻿using Solarbot.Data;
using Solarbot.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Solarbot.Models
{

    public class MemberStatsModel
    {
        public List<int> Levels { get; set; } = new List<int>();
        public List<int> Influences { get; set; } = new List<int>();
        public ulong UserId { get; set; }
        public string Nickname { get; set; }
        public string UserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<string> Dates { get; set; } = new List<string>();

        //helpers
        public MemberStats CurrentMemberStat { get; private set; }

        public MemberStatsModel(IGrouping<ulong, MemberStats> statData, int days)
        {
            var stats = statData
                    .Where(s => s.CreatedDate > DateTime.Now.AddDays(-days))
                    .ToList();

            if (stats.Count() == 0)
                stats = statData.TakeLast(1).ToList();

            //needs at least 2 to compare
            if (stats.Count() == 1)
            {
                stats = Enumerable.Repeat(stats.FirstOrDefault(), 2).ToList();
            }

            UserId = stats.FirstOrDefault().UserId;
            Nickname = stats.FirstOrDefault().Nickname;
            UserName = stats.FirstOrDefault().UserName;
            StartDate = (stats.Any()) ? stats.Min(x => x.CreatedDate) : DateTime.Now;
            EndDate = (stats.Any()) ? stats.Max(x => x.CreatedDate) : DateTime.Now;

            foreach (var stat in stats)
            {
                Levels.Add(stat.Level);
                Influences.Add(stat.Influence);

                if (CurrentMemberStat == null || stat.CreatedDate > CurrentMemberStat.CreatedDate)
                    CurrentMemberStat = stat;
            }
        }

        public MemberStatsModel(List<MemberStats> statlist, int days)
        {
            var stats = statlist
                .Where(s => s.CreatedDate > DateTime.Today.AddDays(-days))
                .ToList();

            if (stats.Count() == 0)
                stats = statlist.TakeLast(1).ToList();

            //needs at least 2 to compare
            if (stats.Count() == 1)
            {
                stats = Enumerable.Repeat(stats.FirstOrDefault(), 2).ToList();
            }

            UserId = stats.FirstOrDefault().UserId;
            Nickname = stats.FirstOrDefault().Nickname;
            UserName = stats.FirstOrDefault().UserName;
            StartDate = (stats.Any()) ? stats.Min(x => x.CreatedDate) : DateTime.Now;
            EndDate = (stats.Any()) ? stats.Max(x => x.CreatedDate) : DateTime.Now;

            foreach (var stat in stats)
            {
                if (CurrentMemberStat == null || stat.CreatedDate > CurrentMemberStat.CreatedDate)
                    CurrentMemberStat = stat;

                Levels.Add(stat.Level);
                Influences.Add(stat.Influence);
                Dates.Add(stat.CreatedDate.ToString("MMM/d"));
            }
        }

        //helpers
        public int LevelProgress
        {
            get
            {
                return (Levels.Any()) ? Levels.Last() - Levels.First() : 0;
            }
        }

        public int InfluenceProgress
        {
            get { return (Influences.Any()) ? Influences.Last() - Influences.First() : 0; }
        }

        public string Name
        {
            get
            {
                string name = (!string.IsNullOrEmpty(Nickname)) ? Nickname : UserName;

                if (name.Length > 18 && name.IndexOf(" ") != -1)
                {
                    // Attempt to reduce the length of the name, should be OK since there's a space.
                    name = name.Substring(0, name.IndexOf(" "));
                }

                if (name.Length > 18)
                {
                    // Name is *still* too long, just force it!
                    name = name.Substring(0, 18);
                }
                return name.FirstLetterToUpperCase()
                    .Replace(".", "") // Only remove "." from names.  It breaks  ```prolog formatting
                    .Sanitize();
            }
        }

        public int PercentOfLevel(int max)
        {
            return (int)((LevelProgress > 0) ? ((double)LevelProgress / max) * 100 : 0);
        }
        public int PercentOfInfluence(int max)
        {
            return (int)((InfluenceProgress > 0) ? ((double)InfluenceProgress / max) * 100 : 0);
        }
        public string DatesText
        {
            get { return $"({StartDate:MMM/d}-{EndDate:MMM/d})"; }
        }

    }

    public static class MemberStatsModelHelpers
    {
        public static List<string> Members(this List<MemberStatsModel> list)
        {
            var names = new List<string>();
            list.ForEach(m => names.Add(m.Name));
            return names;
        }

        public static List<int> LevelPerc(this List<MemberStatsModel> list)
        {
            var max = (list.Any()) ? list.Max(l => l.LevelProgress) : 0;
            var lvls = new List<int>();
            list.ForEach(m => lvls.Add(m.PercentOfLevel(max)));

            //If not enough data to populate list
            for (int i = 0; i + list.Count < 2; i++)
            {
                lvls.Add(0);
            }

            return lvls;
        }

        public static List<int> InfluencePerc(this List<MemberStatsModel> list)
        {
            var max = (list.Any()) ? list.Max(l => l.InfluenceProgress) : 0;
            var infl = new List<int>();
            list.ForEach(m => infl.Add(m.PercentOfInfluence(max)));

            //If not enough data to populate list
            for (int i = 0; i + list.Count < 2; i++)
            {
                infl.Add(0);
            }
            return infl;
        }

        public static List<MemberStatsModel> Order(this IEnumerable<MemberStatsModel> list)
        {
            var lvlMax = (list.Any()) ? list.Max(l => l.LevelProgress) : 0;
            var infMax = (list.Any()) ? list.Max(l => l.InfluenceProgress) : 0;
            return list.OrderByDescending(m => m.PercentOfInfluence(infMax) + m.PercentOfLevel(lvlMax)).ToList();
        }

        public static string Dates(this IEnumerable<MemberStatsModel> list)
        {
            var end = (list.Any()) ? list.Max(l => l.EndDate) : DateTime.Now;
            var start = (list.Any()) ? list.Max(l => l.StartDate) : DateTime.Now;
            return $"({start:MMM/d}-{end:MMM/d})";
        }


    }
}
