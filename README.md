# Solarbot

Hades Star Corporation Management and Stat Bot

## Live Demo and Support

[Join our Discord] (https://discord.gg/M8a72me)

## Documentation

[CloudBot Wiki](https://thecloudbot-theforce.weebly.com/)

[Developer Quick-Start Guide](docs/getting-started.md)

[Developer Compiling/Building Instructions](docs/compiling.md)

