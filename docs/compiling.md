# Developer Compiling and Building Guide

## Windows Instructions

@todo

## Debian Instructions

_Tested on Debian Buster_


### Install Microsoft Mono

Follow [the instructions from the mono project](http://www.mono-project.com/download/stable/#download-lin) to get the basic mono libraries installed.  Particularly `mono-complete` and `monodevelop` are useful.


### Install the Microsoft .NET SDK framework.

```
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg
sudo mv /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-stretch-prod stretch main" | sudo tee /etc/apt/sources.list.d/dotnetdev.list
sudo apt-get update
sudo apt-get install dotnet-sdk
```

Check which version of .NET SDK is available, at the time of writing, 2.1.105 is available

```
apt-cache search dotnet-sdk

sudo apt-get install dotnet-sdk-2.1.105
```


### Library Dependencies

Install the mono libraries that are required to build the project.  This is handled by the nuget module.

```
cd Solarbot
dotnet restore
```


### Compile the Application

Proceeding from here can be one of two options.  The CLI can be used to build the application with the following commands:

```
// Compile a DEBUG release
dotnet publish -r linux-x64 -c debug

-dotnet build -c Debug -r debian-x64-


// Compile a FINAL release
dotnet publish -r linux-x64 -c release

-dotnet build -c Release -r debian-x64-

// (dotnet build seems to only produce a .dll; publish generates a natively-executable binary)
```

These will build a native binary in `Solarbot/bin/[Debug,Release]/netcoreapp2.0/debian-x64/`

Option 2 is to build from within `MonoDevelop`.  To do this, simply double-click on `Solarbot/Solarbot.csproj` to open it in MonoDevelop, and then click `Run`.  This should compile and run the application.


### Common Problems

If dotnet gives a Segmentation Fault after any operation, it may be due to out of date libcrypto libraries.
To confirm if this is the problem, run `gdb --args dotnet restore` then `run` to continue the application; it'll give a stack trace of where the segfault was encountered.  If you see lines such as:

```
Thread 15 "dotnet" received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0x7fff55065700 (LWP 28687)]
0x00007fffe0a8fddd in ?? () from /usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0
```

Try the following to alleviate the problem:

```
sudo mv /usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0 /usr/lib/x86_64-linux-gnu/DISABLED-libcrypto.so.1.0.0
```
