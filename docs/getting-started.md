# Developer Getting Started Guide

## Application Settings

The code ships with an example configuration file in Solarbot called `appsettings.json.default`, copy this to `appsettings.json`.  This file will contain settings required by the application.

## Discord Application

In order to get started, you'll need to create an application in the [official Discord apps page](https://discordapp.com/developers/).  Once logged in, browse to Applications -> My Apps -> New App.  Enter an app name and continue to the next step.  Once there, create a `Bot User` account that the application will use to interact with your users.

Reveal the token and paste that into your `appsettings.json`, this is your private login token for the bot user, **do not** give this out or reveal it, as anyone with it can impersonate your bot!

For production applications, be sure to check `Public Bot` to allow others to add it, otherwise leave it unchecked for development work.

Don't forget to click `Save changes` at the bottom of the page.

## OAuth URL

Once you're ready, click `Generate OAuth2 URL` to create a live URL that you can enable your new bot application to your requested server.