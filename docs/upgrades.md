CREATE TABLE `UserDefaultGuilds` (
	`UserId`	INTEGER NOT NULL,
	`GuildId`	INTEGER NOT NULL,
	PRIMARY KEY(`UserId`)
);

CREATE TABLE `sqlitebrowser_rename_column_new_table` (
	`Id`	INTEGER NOT NULL,
	`LastSeen`	text NOT NULL,
	`Nickname`	text,
	`UserName`	text,
	`GuildId`	bigint NOT NULL DEFAULT 0,
	`TimeZoneId`	text DEFAULT "",
	`ModuleTradeCargo`		INTEGER NOT NULL DEFAULT 0,
	`ModuleMiningBoost`		INTEGER NOT NULL DEFAULT 0,
	`ModuleMiningEnrich`		INTEGER NOT NULL DEFAULT 0,
	`ModuleMiningRemote`		INTEGER NOT NULL DEFAULT 0,
	`ModuleMiningCrunch`		INTEGER NOT NULL DEFAULT 0,
	`ModuleMiningGenesis`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldAlpha`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldDelta`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldPassive`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldOmega`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldMirror`		INTEGER NOT NULL DEFAULT 0,
	`ModuleShieldArea`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportEmp`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportTeleport`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportRepair`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportTime`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportUnity`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportStealth`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportFortify`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportImpulse`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportRocket`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportSalvage`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportSuppress`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportDestiny`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportBarrier`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportVengeance`	INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportLeap`		INTEGER NOT NULL DEFAULT 0,
	`ModuleSupportAlphaDrone`	INTEGER NOT NULL DEFAULT 0,
	`ModuleWeaponBattery`		INTEGER NOT NULL DEFAULT 0,
	`ModuleWeaponLaser`		INTEGER NOT NULL DEFAULT 0,
	`ModuleWeaponMassBattery`	INTEGER NOT NULL DEFAULT 0,
	`ModuleWeaponDualLaser`		INTEGER NOT NULL DEFAULT 0,
	`ModuleWeaponBarrage`		INTEGER NOT NULL DEFAULT 0,
	`ShipBattleship`		INTEGER NOT NULL DEFAULT 0,
	`ShipTransport`			INTEGER NOT NULL DEFAULT 0,
	`ShipMiner`			INTEGER NOT NULL DEFAULT 0,
	`EquippedTransportSupport1`	TEXT NOT NULL DEFAULT "",
	`EquippedMinerSupport1`		TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipWeapon`	TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipShield`	TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipSupport1`	TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipSupport2`	TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipSupport3`	TEXT NOT NULL DEFAULT "",
	`EquippedBattleshipSupport4`	TEXT NOT NULL DEFAULT "",
	`EquippedMinerSupport1`		TEXT NOT NULL DEFAULT "",
	`EquippedMinerMining1`		TEXT NOT NULL DEFAULT "",
	`EquippedMinerMining2`		TEXT NOT NULL DEFAULT "",
	`EquippedMinerMining3`		TEXT NOT NULL DEFAULT "",
	`EquippedMinerMining4`		TEXT NOT NULL DEFAULT "",
	CONSTRAINT `sqlite_autoindex_Users_1` PRIMARY KEY(`Id`,`GuildId`)
);
INSERT INTO sqlitebrowser_rename_column_new_table SELECT `Id`,`LastSeen`,`Nickname`,`UserName`,`GuildId`,`TimeZoneId`, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "", "", "", "", "", "", "", "" FROM `Users`;
PRAGMA defer_foreign_keys
PRAGMA defer_foreign_keys = "1";
DROP TABLE `Users`;
ALTER TABLE `sqlitebrowser_rename_column_new_table` RENAME TO `Users`
PRAGMA defer_foreign_keys = "0";

